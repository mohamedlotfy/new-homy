//
//  AppDelegate.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManager
import GoogleMaps
import GooglePlaces

let googleApiKey = "AIzaSyBO1w7sQQR0sK4WRaGqKm4uVPbjQTbXXD8"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        setUpGoogleMaps()
        
        FirebaseApp.configure()
        initIQKeyboardManager() 
        loadAppData()
        L102Localizer.DoTheMagic()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        User.shared.device_token = deviceTokenString
        User.shared.saveData()
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
    }
    
}

extension AppDelegate {
    
    func setRoot(storyBoard: storyBoardName, vc: storyBoardVCIDs) {
        
        let storyboard = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: vc.rawValue)
        
        self.window?.rootViewController = initialViewController
        
        self.window?.makeKeyAndVisible()
        
    }
}

extension AppDelegate {
    func initIQKeyboardManager() {
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().toolbarBarTintColor = .white
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared().toolbarTintColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 1)
    }
    
    func setUpGoogleMaps(){
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
    }
}

extension AppDelegate {
    func loadAppData() {
        if User.shared.isRegistered() {
            User.shared.loadData()
            User.shared.saveData()
            appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
        }else{
            changeLanguage(storyBoard: storyBoardName.authentication.rawValue, vcId: .login)
            
        }
    }
    
    func changeLanguage(storyBoard: String = "SideMenu", vcId: storyBoardVCIDs) {
        let transition: UIView.AnimationOptions = .transitionCrossDissolve
        
        L102Language.setAppleLAnguageTo(lang: arabicLang)
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
        let storyBoard: UIStoryboard = UIStoryboard.init(name: storyBoard, bundle: nil)
        
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = storyBoard.instantiateViewController(withIdentifier: vcId.rawValue)
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
    }
    
}
