//
//  HomeCVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import Cosmos

class HomeCVCell: UICollectionViewCell {
    
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var servicesImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var discoverButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
}
