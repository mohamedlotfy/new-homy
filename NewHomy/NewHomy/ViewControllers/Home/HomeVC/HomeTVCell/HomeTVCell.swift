//
//  HomeTVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol CompanyCellDelegate: class {
    func didSelect(company: Company)
}


import UIKit

class HomeTVCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cv: UICollectionView!
    
    var companiesList = [Company]()
    var delegate: CompanyCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cv.delegate = self
        cv.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 172 , height: 152)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        self.cv.collectionViewLayout = flowLayout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension HomeTVCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return companiesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath) as! HomeCVCell
        
        let company = companiesList[indexPath.item]
        
        cell.locationLabel.text = company.address
        cell.nameLabel.text = company.name
        
        if let imageLink = URL(string: company.image){
            cell.servicesImageView.kf.setImage(with: imageLink)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(company: companiesList[indexPath.item])
    }
    
}
