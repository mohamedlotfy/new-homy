//
//  NewHomeTVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/24/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class NewHomeTVCell: UITableViewCell {

    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
