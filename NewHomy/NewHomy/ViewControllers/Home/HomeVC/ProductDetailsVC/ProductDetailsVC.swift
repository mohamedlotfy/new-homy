//
//  ProductDetailsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import Cosmos

class ProductDetailsVC: UIViewController {
    
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet weak var productNameNavItem: UINavigationItem!
    
    var product : CompanyProduct?
    var images = [UIImage]()
    var imageSources = [ImageSource]()
    var companyName: String?
    var companyId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpView()
    }
    
    func setUpView(){
        if(product != nil){
            setUpImageSlideShow()
            productNameLabel.text = product!.name
            productNameNavItem.title = product!.name
            descriptionTextView.text = product!.description
            if(companyName != nil){
                companyNameLabel.text = companyName!
            }
            priceLabel.text = product!.price
            rateView.rating = Double(product!.rate)
        }
        if(L102Language.currentAppleLanguage() == arabicLang){
            descriptionTextView.textAlignment = .right
        }
    }
    
    @IBAction func orderButtonClicked(_ sender: UIButton) {
        addToCart()
    }
    
}

extension ProductDetailsVC {
    func setUpImageSlideShow(){
        imageSlideShow.contentScaleMode = .scaleAspectFill
        imageSlideShow.circular = true
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = #colorLiteral(red: 0.8309999704, green: 0.3490000069, blue: 0.2980000079, alpha: 1)
        pageIndicator.pageIndicatorTintColor = #colorLiteral(red: 0.4980392157, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        pageIndicator.backgroundColor = .clear
        
        imageSlideShow.pageIndicator = pageIndicator
        imageSlideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 20))
        getImages()
    }
    
    func getImages (){
        var i = 0
        for image in self.product!.images {
            Alamofire.request(image.image).responseData { (resData) -> Void in
                print(resData.result.value!)
                if(resData.result.value != nil){
                    if let strOutput = UIImage(data: resData.result.value!){
                        DispatchQueue.main.async {
                            self.images.append(strOutput)
                            self.imageSources.append(ImageSource(image: strOutput))
                            i = i + 1
                            if(i == self.imageSources.count){
                                
                                self.imageSlideShow.setImageInputs(self.imageSources)
                            }
                        }
                        
                    }
                }else{
                    print("ERROR")
                }
            }
        }
        
    }
    
    func addToCart() {
        
        DispatchQueue.main.async {
            
            let parameters = ["service_id": self.product!.id,
                              "provider_id": self.companyId]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .cart, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                if error != nil {
                    let alert = UIAlertController(title: "You have items from another company in your cart!".localized, message: "To add Items from this company you will delete your old cart.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Delete".localized, style: .default, handler: { action in
                        self.deleteCart()
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true)

                } else {
                    self.showMessage(sub:  "Added to your cart successfully.".localized, type: .success)
                }
                
            }
        }
        
    }
    
    
    func deleteCart() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            let manager = Manager()
            
            manager.perform(methodType: .get, serviceName: .deleteCart, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    
                } else {
                    
                    self.addToCart()
                }
                
            }
        }
    }

}
