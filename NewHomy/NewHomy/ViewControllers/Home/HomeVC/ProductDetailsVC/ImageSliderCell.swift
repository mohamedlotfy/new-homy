//
//  ImageSliderCell.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/14/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class ImageSliderCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
}
