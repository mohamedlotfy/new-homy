//
//  ServiceDetailsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 11/18/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import ObjectMapper
import BottomPopup

class ServiceDetailsVC: UIViewController, BottomPopupDelegate {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet weak var productNameNavItem: UINavigationItem!
    
    var product : CompanyProduct?
    var images = [UIImage]()
    var imageSources = [ImageSource]()
    var companyName: String?
    var companyId: Int?
    var catId: Int?
    var newAddress = false
    var addressId : String?
    var timeString: String?
    var dateString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(companyId != nil && catId != nil){
            getServiceDetails()
        }
    }
    
    func setUpView(){
        if(product != nil){
            setUpImageSlideShow()
            productNameLabel.text = product!.name
            productNameNavItem.title = product!.name
            descriptionTextView.text = "Type in what you want...".localized
            descriptionTextView.delegate = self
            if(companyName != nil){
                companyNameLabel.text = companyName!
            }
            priceLabel.text = product!.price
        }
        if(L102Language.currentAppleLanguage() == arabicLang){
            descriptionTextView.textAlignment = .right
        }
    }
    
    @IBAction func orderButtonClicked(_ sender: UIButton) {
        if(descriptionTextView.text == "Type in what you want...".localized){
            self.showMessage(sub: "Please, Enter what do you want of this service".localized)
        }else{
            guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "chooseAddressPopUp") as? ChooseAddressPopUpVC else { return }
            popupVC.height = 360
            popupVC.topCornerRadius = 35
            popupVC.presentDuration = 1
            popupVC.dismissDuration = 0.3
            popupVC.popupDelegate = self
            popupVC.delegate = self
            self.present(popupVC, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "dateTime"){
            let vc = segue.destination as! OrderTimePopUpVC
            vc.delegate = self
            vc.isService = true
        }
    }
}

extension ServiceDetailsVC : orderPopUpDelegate{
    func book(dateString: String, timeString: String) {
        self.dateString = dateString
        self.timeString = timeString
        
        if(newAddress){
            getAddresses()
        }else{
            addressId = User.shared.address_id
            makeSeprateOrders()
            
        }
    }
    
    
}

extension ServiceDetailsVC : ChooseAddressPopUpDelegate{
    func didChooseDefaultAddress(_ did: Bool) {
        if(did){
            performSegue(withIdentifier: "dateTime", sender: nil)
        }else{
            let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChooseLocation") as! ChooseLocationVC
            vc.delegate = self
            vc.isFromCart = true
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
}

extension ServiceDetailsVC: ChooseLocationVCDelegate{
    func didChoose(did: Bool) {
        newAddress = did
        performSegue(withIdentifier: "dateTime", sender: nil)
        
    }
    
    
}

extension ServiceDetailsVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Type in what you want...".localized){
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            textView.text = "Type in what you want...".localized
        }
    }
}

extension ServiceDetailsVC {
    func setUpImageSlideShow(){
        imageSlideShow.contentScaleMode = .scaleAspectFill
        imageSlideShow.circular = true
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = #colorLiteral(red: 0.8309999704, green: 0.3490000069, blue: 0.2980000079, alpha: 1)
        pageIndicator.pageIndicatorTintColor = #colorLiteral(red: 0.4980392157, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
        pageIndicator.backgroundColor = .clear
        
        imageSlideShow.pageIndicator = pageIndicator
        imageSlideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .customBottom(padding: 20))
        getImages()
    }
    
    func getImages (){
        var i = 0
        for image in self.product!.images {
            Alamofire.request(image.image).responseData { (resData) -> Void in
                print(resData.result.value!)
                if(resData.result.value != nil){
                    if let strOutput = UIImage(data: resData.result.value!){
                        DispatchQueue.main.async {
                            self.images.append(strOutput)
                            self.imageSources.append(ImageSource(image: strOutput))
                            i = i + 1
                            if(i == self.imageSources.count){
                                
                                self.imageSlideShow.setImageInputs(self.imageSources)
                            }
                        }
                        
                    }
                }else{
                    print("ERROR")
                }
            }
        }
        
    }
    
    
    func getServiceDetails() {
        
        DispatchQueue.main.async {
            
            let manager = Manager()
            
            let url = hostName + "single_service/\(self.catId!)/\(self.companyId!)"
            
            manager.performJson(methodType: .get, useCustomeURL: true, urlStr: url , serviceName: .profile, parameters: nil ) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let companyProduct = Mapper<CompanyProduct>().map(JSONObject: json) {
                        
                        self.product = companyProduct
                        self.setUpView()
                    }
                    
                }
                
            }
        }
        
    }
    
    func makeSeprateOrders(){
        
        DispatchQueue.main.async {
            
            
            let parameters = ["time": self.timeString,
                              "day": self.dateString,
                              "address_id": self.addressId,
                              "notes": self.descriptionTextView.text!,
                              "service_id": self.catId,
                              "provider_id": self.companyId,
                              "type":"service"] as [String : Any]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .orderService, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                if error != nil {
                    self.showMessage(sub: error)
                } else {
                    self.showMessage(sub: "Your order has been placed successfully.".localized ,type: .success)
                    appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                }
            }
        }
    }
    
    func getAddresses() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .address, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    var cartArray = Mapper<Address>().mapArray(JSONObject: json)!
                    cartArray = cartArray.sorted(by: { $0.id > $1.id })
                    
                    if(cartArray.count > 0){
                        self.addressId = "\(cartArray[0].id)"
                    }
                    self.makeSeprateOrders()
                    
                }
                
            }
        }
    }
    
    
}
