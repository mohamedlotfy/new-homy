//
//  CompanyDetailsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class CompanyDetailsVC: UIViewController {
    
    @IBOutlet weak var CV: UICollectionView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyAddress: UILabel!
    
    var catId : Int?
    var selectedCompany : Company?
    var categoriesList = [CompanyProduct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        if(selectedCompany != nil){
            companyNameLabel.text = selectedCompany!.name
            companyAddress.text = selectedCompany!.address
            if(catId != nil){
                getCompanyDetails()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "details"){
            let vc = segue.destination as! ProductDetailsVC
            if let product = sender as? CompanyProduct {
                vc.product = product
                vc.companyName = selectedCompany!.name
                vc.companyId = selectedCompany!.id
            }
        }
    }
    
    
}


extension CompanyDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath) as! HomeCVCell
        
        let company = categoriesList[indexPath.item]
        
        cell.rateView.rating = Double(company.rate)
        cell.locationLabel.text = company.price
        cell.nameLabel.text = company.name
        
        if(company.images.count > 0){
            if let imageLink = URL(string: company.images[0].image){
                cell.servicesImageView.kf.setImage(with: imageLink)
            }
            
        }
        cell.discoverButton.tag = 100 + indexPath.item
        cell.discoverButton.addTarget(self, action: #selector(addToCartButtonClickd(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func addToCartButtonClickd(_ sender: UIButton){
        if(sender.tag >= 100){
            addToCart(serviceId: categoriesList[sender.tag - 100].id)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "details", sender: categoriesList[indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width * 0.5) - 5 , height: 210)
    }
}


extension CompanyDetailsVC {
    func getCompanyDetails() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            let manager = Manager()
            
            let url = hostName + ServiceName.companyDetails.rawValue + "/\(self.selectedCompany!.id)/\(String(describing: self.catId!))"
            
            manager.performJson(methodType: .get, useCustomeURL: true, urlStr: url, serviceName: .companyDetails, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let companyDetails = Mapper<CompanyDetails>().map(JSONObject: json) {
                        
                        self.categoriesList = companyDetails.products
                        self.CV.reloadData()
                    }
                    
                    
                }
                
            }
        }
    }
    
    func addToCart(serviceId: Int) {
        
        DispatchQueue.main.async {
            
            let parameters = ["service_id": serviceId,
                              "provider_id": self.selectedCompany!.id]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .cart, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                if error != nil {
                    let alert = UIAlertController(title: "You have items from another company in your cart!".localized, message: "To add Items from this company you will delete your old cart.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Delete".localized, style: .default, handler: { action in
                        self.deleteCart(serviceId: serviceId)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true)
                } else {
                    self.showMessage(sub:  "Added to your cart successfully.".localized, type: .success)
                }
                
            }
        }
        
    }
    
    func deleteCart(serviceId: Int) {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            let manager = Manager()
            
            manager.perform(methodType: .get, serviceName: .deleteCart, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    
                } else {
                    
                    self.addToCart(serviceId: serviceId)
                }
                
            }
        }
    }
    
}
