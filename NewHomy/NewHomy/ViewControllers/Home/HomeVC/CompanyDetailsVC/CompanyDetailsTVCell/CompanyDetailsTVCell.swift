//
//  CompanyDetailsTVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol ProductCellDelegate: class {
    func didSelect(product: CompanyProduct)
    func didAddToCart(_ did: Bool)
}


import UIKit

class CompanyDetailsTVCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cv: UICollectionView!
    
    var companyId: Int?
    var productList = [CompanyProduct]()
    var delegate: ProductCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cv.delegate = self
        cv.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 172 , height: 152)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        self.cv.collectionViewLayout = flowLayout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CompanyDetailsTVCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath) as! HomeCVCell
        
        let company = productList[indexPath.item]
        
        cell.locationLabel.text = company.price
        cell.nameLabel.text = company.name
        
        if(company.images.count > 0){
            if let imageLink = URL(string: company.images[0].image){
                cell.servicesImageView.kf.setImage(with: imageLink)
            }
            
        }
        cell.discoverButton.tag = 100 + indexPath.item
        cell.discoverButton.addTarget(self, action: #selector(addToCartButtonClickd(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func addToCartButtonClickd(_ sender: UIButton){
        if(sender.tag >= 100){
            addToCart(serviceId: productList[sender.tag - 100].id)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(product: productList[indexPath.item])
    }
    
}

extension CompanyDetailsTVCell{
    func addToCart(serviceId: Int) {
        
        DispatchQueue.main.async {
                        
            let parameters = ["service_id": serviceId,
                              "provider_id": self.companyId]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .cart, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                if error != nil {
                    
                } else {
                    self.delegate?.didAddToCart(true)
                }
                
            }
        }
        
    }

}
