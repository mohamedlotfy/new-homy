//
//  HomeVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeVC: UIViewController {
    
    @IBOutlet weak var TV: UITableView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    var companiesList = [Company]()
    var mainList = ["Companies near you".localized, "Authorized Reseller".localized, "Companies that has offers".localized]
    
    var apiUrl : String?
    var isService: Bool?
    var catId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(apiUrl != nil && isService != nil){
            if(isService!){
                getServiceProviders()
            }else{
                getCompanies()
            }
            
        }
        logoImageView.image = #imageLiteral(resourceName: "Group 7996").withRenderingMode(.alwaysTemplate)
        logoImageView.tintColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "details"){
            let vc = segue.destination as! CompanyDetailsVC
            if let company = sender as? Company{
                vc.selectedCompany = company
                vc.catId = self.catId
            }
        }else if(segue.identifier == "service"){
            let vc = segue.destination as! ServiceDetailsVC
            if let company = sender as? Company{
                vc.catId = self.catId
                vc.companyId = company.id
                vc.companyName = company.name
            }
        }
    }
    
    
}

extension HomeVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return companiesList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewHomeTVCell
        
        let company = companiesList[indexPath.row]
        
        cell.companyAddressLabel.text = company.address
        cell.companyNameLabel.text = company.name
        if let imageLink = URL(string: company.image){
            cell.companyImageView.kf.setImage(with: imageLink)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isService != nil){
            if(isService!){
                performSegue(withIdentifier: "service", sender: companiesList[indexPath.row])
            }else{
                performSegue(withIdentifier: "details", sender: companiesList[indexPath.row])
            }
        }
        
    }
}

extension HomeVC {
    func getCompanies() {
        
        DispatchQueue.main.async {
            
            
            
            self.startAnimating()
            let manager = Manager()
            manager.performJson(methodType: .get, useCustomeURL: true, urlStr: self.apiUrl!, serviceName: .companies, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    
                    let categoriesArray = Mapper<Company>().mapArray(JSONObject: json)!
                    
                    self.companiesList = categoriesArray
                    
                    if(self.companiesList.count > 0){
                        self.TV.isHidden = false
                        self.TV.reloadData()
                    }else{
                        self.TV.isHidden = true
                    }
                    
                }
                
            }
        }
    }
    
    func getServiceProviders() {
        
        DispatchQueue.main.async {
            
            
            
            self.startAnimating()
            let manager = Manager()
            
            let parameters = ["lat": "31.3262822",
                              "lng": "32.1512352"]
            
            manager.performJson(methodType: .post, useCustomeURL: true, urlStr: self.apiUrl!, serviceName: .companies, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    
                    let categoriesArray = Mapper<Company>().mapArray(JSONObject: json)!
                    
                    self.companiesList = categoriesArray
                    
                    if(self.companiesList.count > 0){
                        self.TV.isHidden = false
                        self.TV.reloadData()
                    }else{
                        self.TV.isHidden = true
                    }
                    
                }
                
            }
        }
    }
    
    
}
