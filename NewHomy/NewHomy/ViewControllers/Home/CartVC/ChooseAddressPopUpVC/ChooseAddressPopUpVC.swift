//
//  ChooseAddressPopUpVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/22/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol ChooseAddressPopUpDelegate {
    func didChooseDefaultAddress(_ did: Bool)
}


import UIKit
import BottomPopup
import ObjectMapper

class ChooseAddressPopUpVC:  BottomPopupViewController, UINavigationControllerDelegate  {
    
    @IBOutlet weak var defaultAddressLabel: UILabel!
    @IBOutlet weak var defaultAdressImageView: UIImageView!
    @IBOutlet weak var newAddressImageView: UIImageView!
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var isDefaultAddress = true
    var delegate: ChooseAddressPopUpDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getAddresses()
    }
    
    
    @IBAction func defaultAddressButtonClicked(_ sender: UIButton) {
        if(!isDefaultAddress){
            defaultAdressImageView.image = #imageLiteral(resourceName: "Group 8009")
            newAddressImageView.image = #imageLiteral(resourceName: "Group 1260")
            isDefaultAddress = true
        }
    }
    
    
    @IBAction func newAddressButtonClicked(_ sender: UIButton) {
        if(isDefaultAddress){
            newAddressImageView.image = #imageLiteral(resourceName: "Group 8009")
            defaultAdressImageView.image = #imageLiteral(resourceName: "Group 1260")
            isDefaultAddress = false
        }
    }
    
    @IBAction func chooseButtonClicked(_ sender: UIButton) {
        dismiss(animated: true) {
            self.delegate.didChooseDefaultAddress(self.isDefaultAddress)
        }
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(250)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.2
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
}

extension ChooseAddressPopUpVC{
    func getAddresses() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .address, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    let cartArray = Mapper<Address>().mapArray(JSONObject: json)!
                    for address in cartArray{
                        if(address.is_default == "true"){
                            self.defaultAddressLabel.text = address.description
                        }
                    }
                    
                }
                
            }
        }
    }
    
}
