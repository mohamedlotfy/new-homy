//
//  CartTVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//


protocol CartCellDelegate: class {
    func increseCount(_ did: Bool, forItem: Int)
    func didSelect(_ did: Bool, forItem: Int)
}



import UIKit

class CartTVCell: UITableViewCell {

    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    var qty = 1
    var productSelected = false
    var delegate : CartCellDelegate?
    var cartItemID: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func qtyIncrementButtonClicked(_ sender: UIButton) {
        qty += 1
        qtyLabel.text = "\(qty)"
        delegate?.increseCount(true, forItem: cartItemID ?? 0)
    }
    
    @IBAction func qtyDecrmentButtonClicked(_ sender: UIButton) {
        if(qty > 0){
            qty -= 1
            qtyLabel.text = "\(qty)"
            delegate?.increseCount(false, forItem: cartItemID ?? 0)
        }
    }
    
    @IBAction func selectButtonClicked(_ sender: UIButton) {
        if(productSelected){
            selectButton.setImage(#imageLiteral(resourceName: "Group 1260"), for: .normal)
            productSelected = false
            delegate?.didSelect(false, forItem: cartItemID ?? 0)
        }else{
            selectButton.setImage(#imageLiteral(resourceName: "Group 8009"), for: .normal)
            productSelected = true
            delegate?.didSelect(true, forItem: cartItemID ?? 0)
        }
    }
}
