//
//  CartVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper
import BottomPopup

class CartVC: BaseVC, BottomPopupDelegate {
    
    @IBOutlet weak var emptyCartView: UIView!
    @IBOutlet weak var TV: UITableView!
    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalConstLabel: UILabel!
    
    
    var selectedCategory = -1
    var cartList : Cart?
    var productList = [CartProduct]()
    var carts = [Int]()
    var cartPrices = [(Float,Int)]()
    var total : Float = 0.0
    var newAddress = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.TV.isHidden = true
        self.emptyCartView.isHidden = false
        self.orderButton.isHidden = true
        self.totalLabel.text = "\(total)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.totalLabel.isHidden = true
        self.totalConstLabel.isHidden = true
        self.companyName.isHidden = true
        getCart()
    }
    
    @IBAction func orederButtonClicked(_ sender: UIButton) {
        if(carts.count > 0){
            guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "chooseAddressPopUp") as? ChooseAddressPopUpVC else { return }
            popupVC.height = 360
            popupVC.topCornerRadius = 35
            popupVC.presentDuration = 1
            popupVC.dismissDuration = 0.3
            popupVC.popupDelegate = self
            popupVC.delegate = self
            self.present(popupVC, animated: true, completion: nil)
        }else{
            showMessage(sub: "You must select an item to order first".localized)
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "order"){
            let vc = segue.destination as! OrderTimePopUpVC
            vc.carts = carts
            vc.delegate = self
        }else if(segue.identifier == "payment"){
            let vc = segue.destination as! PaymentVC
            if let dateTimeString = sender as? String {
                vc.carts = carts
                vc.dateString = dateTimeString.components(separatedBy: "T")[0]
                vc.timeString = dateTimeString.components(separatedBy: "T")[0]
                vc.newAddress = self.newAddress
            }
        }
    }
    
    
}

extension CartVC: ChooseAddressPopUpDelegate{
    func didChooseDefaultAddress(_ did: Bool) {
        if(did){
            performSegue(withIdentifier: "order", sender: nil)
        }else{
            let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChooseLocation") as! ChooseLocationVC
            vc.delegate = self
            vc.isFromCart = true
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

extension CartVC: ChooseLocationVCDelegate{
    func didChoose(did: Bool) {
        newAddress = did
        if(did){
            performSegue(withIdentifier: "order", sender: nil)
        }
    }
    
    
}

extension CartVC: orderPopUpDelegate{
    func book(dateString: String, timeString: String) {
        
        performSegue(withIdentifier: "payment", sender: "\(dateString)T\(timeString)")
        
    }
    
    
}

extension CartVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartTVCell
        
        let product = productList[indexPath.row]
        cell.serviceNameLabel.text = product.name
        cell.costLabel.text = "\(product.price)"
        cell.qty = Int(product.count) ?? 1
        cell.qtyLabel.text = product.count
        if(product.images.count > 0){
            if let imageLink = URL(string: product.images[0].image){
                cell.serviceImageView.kf.setImage(with: imageLink)
            }
        }
        
        var index = 0
        
        for cart in carts {
            if(product.id == cart){
                cell.productSelected = true
                index = 1
            }
        }
        
        if(index == 0){
            cell.productSelected = false
        }
        
        if(cell.productSelected){
            cell.selectButton.setImage(#imageLiteral(resourceName: "Group 8009"), for: .normal)
        }else{
            cell.selectButton.setImage(#imageLiteral(resourceName: "Group 1260"), for: .normal)
        }
        
        cell.delegate = self
        cell.cartItemID = product.id
        cell.deleteButton.tag = 100 + indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func deleteButtonClicked(_ sender: UIButton){
        if(sender.tag >= 100){
            let alert = UIAlertController(title: "Alert".localized, message: "Are you sure you want delete this item?".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "DELETE".localized, style: .destructive, handler: { action in
                self.deleteCart(forItemID: self.productList[sender.tag - 100].id)
                self.cartList!.products.remove(at: sender.tag - 100)
                self.productList.remove(at: sender.tag - 100)
                
            }))
            alert.addAction(UIAlertAction(title: "CANCEL".localized, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
}
//
//extension CartVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return cartList.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CartCVCell
//
//        let cart = cartList[indexPath.item]
//
//        cell.categoryLabel.text = cart.category
//        if(selectedCategory == indexPath.item){
//            cell.categoryView.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 1)
//            cell.categoryLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        }else{
//            cell.categoryView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//            cell.categoryLabel.textColor = #colorLiteral(red: 0.5125125051, green: 0.5125125051, blue: 0.5125125051, alpha: 1)
//        }
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.selectedCategory = indexPath.item
//        self.productList = cartList[indexPath.item].products
//        self.CV.reloadData()
//        self.TV.reloadData()
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: Int(collectionView.frame.width) / self.cartList.count , height: 43)
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//
//
//
//}

extension CartVC {
    func getCart() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .cart, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    if let cart = Mapper<Cart>().map(JSONObject: json) {
                        
                        self.cartList = cart
                        if(cart.products.count > 0){
                            self.TV.isHidden = false
                            self.emptyCartView.isHidden = true
                            self.productList = cart.products
                            self.TV.reloadData()
                            self.totalLabel.isHidden = false
                            self.companyName.isHidden = false
                            self.totalConstLabel.isHidden = false
                            self.companyName.text = cart.providerName
                            self.getTotal()
                        }else{
                            self.TV.isHidden = true
                            self.emptyCartView.isHidden = false
                            self.totalLabel.isHidden = true
                            self.companyName.isHidden = true
                            self.totalConstLabel.isHidden = true
                        }
                    }
                }
                
            }
        }
    }
    
    func changeCount(increase: Bool, forItemID: Int) {
        
        DispatchQueue.main.async {
            
            var service : ServiceName?
            if(increase){
                service = .increaseCart
            }else{
                service = .decreaseCart
            }
            
            let manager = Manager()
            manager.perform(methodType: .get,arguments: "\(forItemID)",serviceName: service!, parameters: nil) { (json, error) -> Void in
                
                if let err = error {
                    self.showMessage(sub: err)
                }else{
                    self.getCart()
                }
            }
        }
    }
    
    func deleteCart(forItemID: Int) {
        
        DispatchQueue.main.async {
            
            
            let manager = Manager()
            manager.perform(methodType: .delete,arguments: "\(forItemID)",serviceName: .cart, parameters: nil) { (json, error) -> Void in
                
                if let err = error {
                    self.showMessage(sub: err)
                }else{
                    self.TV.reloadData()
                }
            }
        }
    }
    
}

extension CartVC: CartCellDelegate{
    func increseCount(_ did: Bool, forItem: Int) {
        self.changeCount(increase: did, forItemID: forItem)
    }
    
    func didSelect(_ did: Bool, forItem: Int) {
        
        if(did){
            carts.append(forItem)
            orderButton.isHidden = false
            getTotal()
        }else{
            if(carts.count > 0){
                if(carts.count > 1){
                    orderButton.isHidden = false
                }else{
                    orderButton.isHidden = true
                    
                }
                var index = 0
                for cart in carts{
                    if cart == forItem{
                        carts.remove(at: index)
                        getTotal()
                        return
                    }
                    index = index + 1
                }
            }
        }
    }
    
    func getTotal(){
        total = 0.0
        for product in productList{
            for cart in carts{
                if(cart == product.id){
                    total = total + (Float(product.price) * Float(product.count)!)
                }
            }
        }
        totalLabel.text = "\(total)"
    }
    
}
