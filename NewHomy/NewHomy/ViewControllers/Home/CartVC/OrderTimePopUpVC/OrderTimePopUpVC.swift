//
//  OrderTimePopUpVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol orderPopUpDelegate {
    func book(dateString: String, timeString: String)
}


import UIKit

class OrderTimePopUpVC: UIViewController {
    
    @IBOutlet weak var timePickerView: UIDatePicker!
    
    var carts = [Int]()
    var isService = false
    var delegate: orderPopUpDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        timePickerView.minimumDate = Date()
    }
    
    @IBAction func bookButtonClicked(_ sender: UIButton) {
        if(isService){
            delegate.book(dateString: self.timePickerView.date.formatToSend, timeString: self.timePickerView.date.timeString)
        }else{
            if(carts.count > 0){
                delegate.book(dateString: self.timePickerView.date.formatToSend, timeString: self.timePickerView.date.timeString)
            }
        }
        self.dismiss(animated: true, completion: nil)

    }
    
}
