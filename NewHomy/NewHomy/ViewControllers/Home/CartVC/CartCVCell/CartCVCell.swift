//
//  CartCVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class CartCVCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryView: UIView!
    
}
