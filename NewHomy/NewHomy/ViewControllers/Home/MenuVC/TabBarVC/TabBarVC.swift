//
//  TabBarVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import SWRevealViewController

class TabBarVC: UITabBarController,SWRevealViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    let dimView = UIView()
    let menuButton = UIButton()
    let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        revealViewController().delegate = self
        createDimView()
        
        menuButton.frame.size = CGSize(width: 78, height: tabBar.bounds.height)
        menuButton.backgroundColor = .clear
        if(L102Language.currentAppleLanguage() == arabicLang){
            menuButton.center = CGPoint(x: 39, y: tabBar.bounds.height / 2)
        }else{
            menuButton.center = CGPoint(x: UIScreen.main.bounds.width - 39, y: tabBar.bounds.height / 2)

        }
        
        menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        tabBar.addSubview(menuButton)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        revealViewController().delegate = self
        
        if revealViewController() != nil {
            //            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            //            self.revealViewController().panGestureRecognizer().isEnabled = true
        }
        integrateSideMenu()
    }
    
    func integrateSideMenu() {
        if revealViewController() != nil {
            
            if L102Language.currentAppleLanguage() == englishLang {
                menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                revealViewController().rearViewRevealWidth = self.view.frame.size.width / 1.3
                revealViewController().rightViewRevealWidth = 0
                revealViewController().rightViewRevealOverdraw = 0
                revealViewController().rearViewRevealOverdraw = 0
            }
            else{
                menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
                revealViewController().rightViewRevealWidth = self.view.frame.size.width / 1.3
                revealViewController().rightViewRevealOverdraw = 0
                revealViewController().rearViewRevealWidth = 0
                revealViewController().rearViewRevealOverdraw = 0
            }
        }
    }
    
    func createDimView(){
        dimView.frame = CGRect.init(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        dimView.backgroundColor = .clear
        self.view.addSubview(dimView)
        dimView.isHidden = true
    }
    
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if(position == .left) {
            dimView.isHidden = true
        }
        else {
            dimView.isHidden = false
            self.view.endEditing(true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
