//
//  MenuVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/14/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import SWRevealViewController

class MenuVC: UIViewController {
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var didSelectRow:Bool? = false
    static var selectedRow : Int? = 0
    
    // MARK: Menu table content
    let menuItems = [("My Orders".localized,#imageLiteral(resourceName: "Group 1189")),("My addresses".localized,#imageLiteral(resourceName: "Group 1255")),("Settings".localized,#imageLiteral(resourceName: "Group 1190")),("Complaints and sugestions".localized,#imageLiteral(resourceName: "Group 1191")),("Logout".localized,#imageLiteral(resourceName: "Group 1192")),("Terms and Conditions".localized,#imageLiteral(resourceName: "union4")),("Privacy Policy".localized,#imageLiteral(resourceName: "union4")),("FAQ".localized,#imageLiteral(resourceName: "Search field"))]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillData()
        
    }
    
    @IBAction func profileButtonClicked(_ sender: UIButton) {
        setMenuFrontView(storyBoard: .sideMenu, vc: .profile)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillData()
        
    }
    
    func fillData(){
        // fill image and name label
        if(User.shared.username != ""){
            userNameLabel.text = User.shared.username
        }
        
        if(User.shared.profileImage != ""){
            if let imageLink = URL(string: User.shared.profileImage ?? ""){
                userProfileImageView.kf.setImage(with: imageLink)
            }
        }
    }
    
    
    
}


extension MenuVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < 5){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTVCell
            
            cell.menuLabel.text = menuItems[indexPath.row].0
            cell.menuImageView.image = menuItems[indexPath.row].1
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
            cell.textLabel?.text =  menuItems[indexPath.row].0
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            setMenuFrontView(storyBoard: .sideMenu, vc: .orders)
        case 1:
            setMenuFrontView(storyBoard: .sideMenu, vc: .address)
        case 2:
            setMenuFrontView(storyBoard: .sideMenu, vc: .settings)
        case 3:
            setMenuFrontView(storyBoard: .sideMenu, vc: .complaints)
        case 4:
            logout()
        default:
            break
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
}

extension MenuVC{
    func logout() {
        
        DispatchQueue.main.async {
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .logout, parameters: nil) { (json, error) -> Void in
                
                if let err = error {
                    self.showMessage(sub: err)
                }else{
                    User.shared.logout()
                    User.shared.saveData()
                    appDelegate.setRoot(storyBoard: .authentication, vc: .login)
                }
            }
        }
    }
}
