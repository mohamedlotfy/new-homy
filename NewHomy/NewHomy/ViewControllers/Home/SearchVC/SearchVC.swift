//
//  SearchVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchVC: BaseVC {
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var CV: UICollectionView!
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var noResultImageView: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    
    var productList = [CompanyProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        emptyView.isHidden = false
        noResultLabel.text = "Type in what you are looking for!".localized
        noResultImageView.isHidden = true
        searchTF.returnKeyType = .search
        searchTF.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.searchTF.becomeFirstResponder()
        
    }
    
    @IBAction func searchValueChanged(_ sender: UITextField) {
        if(searchTF.text != ""){
            
            var clearImage = #imageLiteral(resourceName: "Close button")
            clearImage = clearImage.withRenderingMode(.alwaysTemplate)
            searchButton.setImage(clearImage, for: .normal)
            searchButton.tintColor = #colorLiteral(red: 0.5803921569, green: 0.5764705882, blue: 0.6, alpha: 1)
            searchButton.tag = 10
        }else{
            searchButton.setImage(#imageLiteral(resourceName: "Path 1443"),for: .normal)
            searchButton.tag = 0
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func SearchButtonClicked(_ sender: UIButton) {
        if(sender.tag == 0){
            if(!searchTF.text!.isEmpty){
                searchFor(searchTF.text!)
            }
        }else if(sender.tag == 10){
            searchTF.text = ""
            emptyView.isHidden = false
            noResultLabel.text = "Type in what you are looking for!".localized
            noResultImageView.isHidden = true
            CV.isHidden = true
            searchButton.setImage(#imageLiteral(resourceName: "Path 1443"),for: .normal)
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "details"){
            let vc = segue.destination as! ProductDetailsVC
            if let product = sender as? CompanyProduct {
                vc.product = product
                vc.companyName = product.companyName
                vc.companyId = product.companyId
            }
        }
    }
    
    
}

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCVCell
        
        let product = productList[indexPath.item]
        cell.nameLabel.text = product.name
        cell.locationLabel.text = product.price
        if(product.images.count > 0){
            if let imageLike = URL(string: product.images[0].image){
                cell.servicesImageView.kf.setImage(with: imageLike)
            }
        }
        
        cell.discoverButton.tag = 100 + indexPath.item
        cell.discoverButton.addTarget(self, action: #selector(addToCartButtonClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func addToCartButtonClicked(_ sender: UIButton){
        if(sender.tag >= 100){
            addToCart(product: productList[sender.tag - 100])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width/2 - 5 , height: 170)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "details", sender: productList[indexPath.item])
    }
    
    
}


extension SearchVC {
    func searchFor(_ word: String) {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            let parameters = ["search": word]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .search, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    let productsArray = Mapper<CompanyProduct>().mapArray(JSONObject: json)!
                    
                    self.productList = productsArray
                    if(self.productList.count > 0){
                        self.CV.isHidden = false
                        self.emptyView.isHidden = true
                        self.CV.reloadData()
                    }else{
                        self.CV.isHidden = true
                        self.emptyView.isHidden = false
                        self.noResultImageView.isHidden = false
                        self.noResultLabel.text = "No result found!".localized
                    }
                }
                
            }
        }
    }
    
    func addToCart(product: CompanyProduct) {
        
        DispatchQueue.main.async {
            
            let parameters = ["service_id": product.id,
                              "provider_id": product.companyId]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .cart, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                if error != nil {
                    
                } else {
                    self.showMessage(sub: "Added to your cart successfully.".localized, type: .success)
                }
                
            }
        }
        
    }
    
}

extension SearchVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if(textField == searchTF){
            if(!searchTF.text!.isEmpty){
                searchFor(searchTF.text!)
            }
        }
        return true
    }
}
