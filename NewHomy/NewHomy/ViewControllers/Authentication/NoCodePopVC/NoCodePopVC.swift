//
//  NoCodePopVC.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

protocol NoCodeDelegete {
    func resendCode(_ send: Bool)
    func changeNumber(_ change: Bool)
}


import UIKit
import BottomPopup
class NoCodePopVC: BottomPopupViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var delegate: NoCodeDelegete!
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(phoneNumber != nil){
            phoneLabel.text = phoneNumber!
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func resendCodeButtonClicked(_ sender: UIButton) {
        delegate.resendCode(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeNumberButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate.changeNumber(true)
        }
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(250)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.2
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
