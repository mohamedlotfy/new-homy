//
//  SaveAddressPopUpVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol saveAddressDelegate {
    func save(isDefault: Bool, addess: String, lat: String, lng: String)
}


import UIKit
import BottomPopup
import ObjectMapper

class SaveAddressPopUpVC: BottomPopupViewController, UINavigationControllerDelegate  {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var defaultAddressImageView: UIImageView!
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var delegate : saveAddressDelegate!
    
    var address: String?
    var lat: String?
    var lng: String?
    var isDefault = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        addressLabel.text = address
       
    }
    
    @IBAction func defaultAddressButtonClicked(_ sender: UIButton) {
        isDefault = !isDefault
        if(isDefault){
            defaultAddressImageView.image = #imageLiteral(resourceName: "Group 8009")
        }else{
            defaultAddressImageView.image = #imageLiteral(resourceName: "Group 1260")
        }
    }
    
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        delegate.save(isDefault: isDefault, addess: address!, lat: lat!, lng: lng!)
        dismiss(animated: true, completion: nil)
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(250)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.2
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
}
