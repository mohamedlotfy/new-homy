//
//  ChooseLocationVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//
protocol ChooseLocationVCDelegate {
    func didChoose(did: Bool)
}

import UIKit
import GoogleMaps
import GooglePlaces
import BottomPopup
import ObjectMapper

class ChooseLocationVC:  UIViewController, BottomPopupDelegate {
    
    @IBOutlet weak var backButton: BackBarButtonItem!
    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager:CLLocationManager!
    var userCordinates : CLLocationCoordinate2D?
    
    var userAddress : String?
    
    var delegate: ChooseLocationVCDelegate!
    
    var isFromCart: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mapView.delegate = self
        self.transparentNavBar()
        determineMyCurrentLocation()
        if(User.shared.status == "register"){
            self.backButton.isEnabled = false
            self.backButton.image = UIImage()
        }
    }
    
    @IBAction func searchDidBeginEdit(_ sender: UITextField) {
        focusOnEgypt()
        
    }
    
    @IBAction func findMeButtonClicked(_ sender: UIButton) {
        reverseGeocodeCoordinate(mapView.camera.target)
    }
    
}

extension ChooseLocationVC: CLLocationManagerDelegate{
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        // 7
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        // 8
        locationManager.stopUpdatingLocation()
    }
}

extension ChooseLocationVC: GMSMapViewDelegate{
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            // 3
            self.userAddress = lines.joined(separator: "\n")
            
            // 4
            
            guard let popupVC = self.storyboard?.instantiateViewController(withIdentifier: "saveAddress") as? SaveAddressPopUpVC else { return }
            popupVC.height = 350
            popupVC.topCornerRadius = 35
            popupVC.presentDuration = 1
            popupVC.dismissDuration = 0.3
            popupVC.popupDelegate = self
            popupVC.address = self.userAddress
            popupVC.delegate = self
            popupVC.lat = coordinate.latitude.description
            popupVC.lng = coordinate.longitude.description
            self.present(popupVC, animated: true, completion: nil)
        }
    }
}

extension ChooseLocationVC: GMSAutocompleteViewControllerDelegate {
    
    func focusOnEgypt(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment  //suitable filter type
        filter.country = "EG"
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("lat",place.coordinate.latitude)
        print("lon",place.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude), zoom: 17.0)
        DispatchQueue.main.async
            {
                // 2. Perform UI Operations.
                let position = CLLocationCoordinate2DMake(place.coordinate.latitude,place.coordinate.longitude)
                let marker = GMSMarker(position: position)
                marker.icon = UIImage(named:"Group 1255")
                marker.map = self.mapView
        }
        
        self.mapView?.animate(to: camera)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


extension ChooseLocationVC: saveAddressDelegate {
    func save(isDefault: Bool, addess: String, lat: String, lng: String) {
        
        DispatchQueue.main.async {
            
            let manager = Manager()
            
            let parameters = ["lat": lat,
                              "lng": lng,
                              "title": "title",
                              "description": addess,
                              "is_default": isDefault.description]
            
            
            manager.perform(methodType: .post, serviceName: .address, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                                        
                    if let user = Mapper<User>().map(JSONObject: json) {
                        
                        User.shared.fillUserModel(model: user)
                        
                        User.shared.saveData()
                        
                    }
                    
                    if(self.isFromCart){
                        self.delegate.didChoose(did: true)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                    }
                    
                }
                
            }
        }
    }
    
}
