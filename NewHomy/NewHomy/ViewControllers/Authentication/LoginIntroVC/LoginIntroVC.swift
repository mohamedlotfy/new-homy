//
//  LoginIntroVC.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class LoginIntroVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loginView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginView.semanticContentAttribute = .forceLeftToRight
        if(L102Language.currentAppleLanguage() == arabicLang){
            titleLabel.textAlignment = .right
        }else{
            titleLabel.textAlignment = .left
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
          self.navigationController?.navigationBar.isHidden = true
      }
      
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
