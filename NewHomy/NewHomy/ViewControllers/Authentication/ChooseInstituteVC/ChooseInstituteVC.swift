//
//  ChooseInstituteVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class ChooseInstituteVC: UIViewController {

    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var individualLabel: UILabel!
    
    var selectedValue = ""
    var selectedSubCategory : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    

    @IBAction func typeButtonClicked(_ sender: UIButton) {
        if(sender.tag == 10){
            // company
            selectedValue = "company"
            companyLabel.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 0.3413366866)
            individualLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3413366866)
            
        }else if(sender.tag == 11){
            // individual
            selectedValue = "person"
            individualLabel.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 0.3413366866)
            companyLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3413366866)
            
        }
        performSegue(withIdentifier: "companies", sender: nil)
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        if(selectedValue != ""){
            // navigate to tab bar
            performSegue(withIdentifier: "companies", sender: nil)
        }else{
            showMessage(sub: "Please, Choose institute type first".localized)
        }
    }
    
  

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "companies"){
            let vc = segue.destination as! HomeVC
            vc.apiUrl = hostName + "providers/\(String(describing: selectedSubCategory!))/\(selectedValue)"
            vc.isService = true
            vc.catId = selectedSubCategory!
        }
    }


}
