//
//  ChooseSubSubCategoryVC.swift
//  NewHomy
//
//  Created by haniielmalky on 11/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class ChooseSubSubCategoryVC: UIViewController {
    
    @IBOutlet weak var TV: UITableView!
    var selectedIndex = -1
    
    var selectedSubCategoryId: Int?
    
    var subCategoriesList = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(selectedSubCategoryId != nil){
            getCategories()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
  
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if(segue.identifier == "companies"){
            let vc = segue.destination as! HomeVC
            if let id = sender as? Int{
                vc.isService = false
                vc.apiUrl = hostName + ServiceName.companies.rawValue + "/\(id)"
                vc.catId = id
                
            }
        }
     }
     
    
}

extension ChooseSubSubCategoryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChooseServiceTVCell
        
        cell.selectionStyle = .none
        
        let category = subCategoriesList[indexPath.row]
        
        cell.serviceNameLabel.text = category.name
        
        if let imageLink = URL(string: category.image){
            cell.serviceImageView.kf.setImage(with: imageLink)
        }
        
        if(selectedIndex == indexPath.row){
            cell.serviceNameLabel.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 0.3413366866)
        }else{
            cell.serviceNameLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3413366866)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.layer.frame.height / 2.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "companies", sender: subCategoriesList[indexPath.row].id)
        
    }
    
    
}

extension ChooseSubSubCategoryVC {
    func getCategories() {
        
        DispatchQueue.main.async {
            self.startAnimating()
            let manager = Manager()
            manager.perform(methodType: .get,arguments: "\(self.selectedSubCategoryId ?? 0)", serviceName: .subCategory, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    
                    let categoriesArray = Mapper<Category>().mapArray(JSONObject: json)!
                    
                    self.subCategoriesList = categoriesArray
                    self.TV.reloadData()
                    
                    
                }
                
            }
        }
    }
    
}

