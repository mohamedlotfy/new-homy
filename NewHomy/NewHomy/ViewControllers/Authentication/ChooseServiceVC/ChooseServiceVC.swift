//
//  ChooseServiceVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class ChooseServiceVC: BaseVC {
    
    @IBOutlet weak var TV: UITableView!
    
    var categoriesList = [Category]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        if(selectedIndex != -1){
            if(categoriesList[selectedIndex].type == "service"){
                performSegue(withIdentifier: "next", sender: nil)
            }else if(categoriesList[selectedIndex].type == "material"){
                performSegue(withIdentifier: "companies", sender: nil)
            }
        }else{
            showMessage(sub: "Please, Choose service first".localized)
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "next"){
            let vc = segue.destination as! ChooseSubServiceVC
            vc.selectedServiceId = self.categoriesList[selectedIndex].id
        }
    }
    
    
}

extension ChooseServiceVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChooseServiceTVCell
        
        cell.selectionStyle = .none
        
        let category = categoriesList[indexPath.row]
        
        cell.serviceNameLabel.text = category.name
        
        if let imageLink = URL(string: category.image){
            cell.serviceImageView.kf.setImage(with: imageLink)
        }
        
        if(selectedIndex == indexPath.row){
            cell.serviceNameLabel.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.3490196078, blue: 0.2980392157, alpha: 0.3413366866)
        }else{
            cell.serviceNameLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3413366866)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        if(categoriesList[selectedIndex].type == "service"){
            performSegue(withIdentifier: "next", sender: nil)
        }else if(categoriesList[selectedIndex].type == "material"){
            performSegue(withIdentifier: "companies", sender: nil)
        }
    }
    
    
}

extension ChooseServiceVC {
    func getCategories() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .category, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    
                    let categoriesArray = Mapper<Category>().mapArray(JSONObject: json)!
                    
                    self.categoriesList = categoriesArray
                    self.TV.reloadData()
                    
                    
                }
                
            }
        }
    }
    
}
