//
//  LoginVC.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import FirebaseAuth
import ObjectMapper

class LoginVC: UIViewController {
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setLogic()
        phoneView.semanticContentAttribute = .forceLeftToRight
        
        
        if(L102Language.currentAppleLanguage() == arabicLang){
            phoneTitleLabel.textAlignment = .right
        }else{
            phoneTitleLabel.textAlignment = .left
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        phoneTF.textAlignment = .left
        phoneTF.makeTextWritingDirectionLeftToRight(phoneTF)
        phoneTF.semanticContentAttribute = .forceLeftToRight
        self.phoneTF.becomeFirstResponder()
        
    }
    
    
    func setLogic(){
        self.phoneTF.becomeFirstResponder()
    }
    
    @IBAction func phoneTextFieldButtonClicked(_ sender: UIButton) {
        self.phoneTF.becomeFirstResponder()
        
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if isNotEmptyString(text: self.phoneTF.text!, withAlertMessage: "Please, insert phone number".localized) {
            checkNumber()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LoginVC{
    
    func checkNumber() {
        
        DispatchQueue.main.async {
            
            let parameters = ["mobile": "966" + self.phoneTF.text! ]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .checkNumber, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    if let response = json as? Bool{
                        if(response){
                            self.login()
                        }else{
                            self.startAnimating()
                            PhoneAuthProvider.provider().verifyPhoneNumber("+966" + self.phoneTF.text! , uiDelegate: nil) { (verifID, error) in
                                if let error = error {
                                    self.stopAnimating()
                                    self.showMessage(sub: error.localizedDescription)
                                    return
                                    
                                }
                                self.stopAnimating()
                                
                                let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
                                let vc  = storyboard.instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
                                vc.phoneNumber = "+966" + self.phoneTF.text!
                                vc.verificationID = verifID
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            
                            
                        }
                    }
                }
                
            }
        }
        
    }
    
    func login() {
        
        DispatchQueue.main.async {
            
            let parameters = ["mobile": "966" + self.phoneTF.text! ,
                              "device_token": User.shared.device_token ?? "12"]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .login, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let user = Mapper<User>().map(JSONObject: json) {
                        
                        User.shared.fillUserModel(model: user)
                        
                        User.shared.setIsRegister(registered: true)
                        
                        User.shared.saveData()
                        
                        if(User.shared.status == "login"){
                            appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                        }else if(User.shared.status == "register"){
                            let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChooseLocation")
                            self.navigationController?.navigationBar.isHidden = false
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }
                    
                }
                
            }
        }
        
    }
    
}

