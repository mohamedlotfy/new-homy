//
//  ChooseSubServiceVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/9/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class ChooseSubServiceVC: UIViewController {

    @IBOutlet weak var TV: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    
    var selectedIndex = -1
    var selectedServiceId: Int?
    var subCategoriesList = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        if(selectedIndex != -1){
            performSegue(withIdentifier: "next", sender: nil)
        }else{
            showMessage(sub: "Please, Choose service first".localized)
        }
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "next"){
            let vc = segue.destination as! ChooseInstituteVC
            if let id = sender as? Int {
                vc.selectedSubCategory = id
            }
        }
    }

}

extension ChooseSubServiceVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChooseServiceTVCell
        
        cell.selectionStyle = .none
        
        let category = subCategoriesList[indexPath.row]
        
        cell.serviceNameLabel.text = category.name
        
        if(category.images.count > 0){
            if let imageLink = URL(string: category.images[0].image){
                cell.serviceImageView.kf.setImage(with: imageLink)
            }
        }
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        performSegue(withIdentifier: "next", sender: subCategoriesList[indexPath.row].id)
    }
    
   
}

extension ChooseSubServiceVC {
    func getCategories() {
        
        DispatchQueue.main.async {
            self.startAnimating()
            let manager = Manager()
            manager.perform(methodType: .get, arguments: "\(self.selectedServiceId!)", serviceName: .category, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    
                    let categoriesArray = Mapper<Category>().mapArray(JSONObject: json)!
                    
                    self.subCategoriesList = categoriesArray
                    self.TV.reloadData()
                    
                    
                }
                
            }
        }
    }
    
}

