//
//  CodeVC.swift
//  NewHomy
//
//  Created by Mohamed Lotfy on 10/10/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import BottomPopup
import FirebaseAuth
import ObjectMapper

class CodeVC: UIViewController {
    
    @IBOutlet weak var digitInputView: DigitInputView! {
        didSet {
            self.digitInputView.numberOfDigits = 6
            self.digitInputView.textColor = .black
            self.digitInputView.bottomBorderColor = .black
            self.digitInputView.nextDigitBottomBorderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.digitInputView.textField?.keyboardAppearance = .default
            self.digitInputView.acceptableCharacters = "0123456789"
            self.digitInputView.keyboardType = .asciiCapableNumberPad
            self.digitInputView.textField?.textContentType = .oneTimeCode
            //            self.digitInputView.font = UIFont.monospacedDigitSystemFont(ofSize: 4, weight: UIFont.Weight(rawValue: 1))
            self.digitInputView.font = .systemFont(ofSize: 20, weight: .semibold)
            self.digitInputView.animationType = .spring
            if #available(iOS 12.0, *) {
                self.digitInputView.textField?.textContentType = .oneTimeCode
            } else {
                
            }
        }
    }
    
    
    var phoneNumber : String?
    var verificationID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        _ = self.digitInputView.becomeFirstResponder()
        
    }
    
  
    @IBAction func codeDoneClicked(_ sender: Any) {
        
        if(isNotEmptyString(text: digitInputView.text, withAlertMessage: "Please, Insert the verification code")){
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: self.verificationID!,
                verificationCode: self.digitInputView.text)
            self.startAnimating()
            Auth.auth().signIn(with: credential) { (authResult, error) in
                
                if let error = error {
                    self.stopAnimating()
                    self.showMessage(sub: error.localizedDescription)
                    return
                }
                self.login()
                
            }
        }
        
    }
    
    
    @IBAction func noCodeClicked(_ sender: Any) {
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "NoCodePopVC") as? NoCodePopVC else { return }
        popupVC.height = 400
        popupVC.topCornerRadius = 35
        popupVC.presentDuration = 1
        popupVC.dismissDuration = 0.3
        popupVC.popupDelegate = self
        popupVC.delegate = self
        popupVC.phoneNumber = self.phoneNumber!
        present(popupVC, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension CodeVC: BottomPopupDelegate {
    
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}

extension CodeVC{
    
    func login() {
        
        DispatchQueue.main.async {
            
            let parameters = ["mobile": self.phoneNumber ?? "",
                              "device_token": User.shared.device_token ?? "12"]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .login, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let user = Mapper<User>().map(JSONObject: json) {
                        
                        User.shared.fillUserModel(model: user)
                        
                        User.shared.setIsRegister(registered: true)
                        
                        User.shared.saveData()
                        
                        if(User.shared.status == "login"){
                            appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                        }else if(User.shared.status == "register"){
                            let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChooseLocation")
                            self.navigationController?.navigationBar.isHidden = false
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }
                    
                }
                
            }
        }
        
    }
    
}

extension CodeVC: NoCodeDelegete{
    func resendCode(_ send: Bool) {
        if(send){
            self.startAnimating()
            PhoneAuthProvider.provider().verifyPhoneNumber(self.phoneNumber! , uiDelegate: nil) { (verifID, error) in
                if let error = error {
                    self.stopAnimating()
                    self.showMessage(sub: error.localizedDescription)
                    return
                    
                }
                self.stopAnimating()
                self.showMessage(sub: "Code Sent Successfully.".localized,type: .success )
            }
        }
    }
    
    func changeNumber(_ change: Bool) {
        if(change){
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    
}
