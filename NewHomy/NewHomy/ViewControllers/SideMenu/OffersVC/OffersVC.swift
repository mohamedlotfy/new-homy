//
//  OffersVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/24/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class OffersVC: BaseVC {

    @IBOutlet weak var TV: UITableView!
    
    var offersList = [Offer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getOffers()
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "product"){
            let vc = segue.destination as! ProductDetailsVC
            if let offer = sender as? Offer {
                vc.product = offer.service
                vc.companyName = offer.companyName
                vc.companyId = offer.companyId
                
            }
        }
    }
    

}

extension OffersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OffersTVCell
        
        let offer = offersList[indexPath.row]
        
        cell.offerPercentageLabel.text = offer.discount + "%"
        cell.offerDescriptionLabel.text = offer.description
        cell.offerNameLabel.text = offer.name
        if let imageLink = URL(string: offer.imageLink){
            cell.offerImageView.kf.setImage(with: imageLink)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "product", sender: offersList[indexPath.row])
    }
}

extension OffersVC{
    func getOffers(){
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .offer, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    let cartArray = Mapper<Offer>().mapArray(JSONObject: json)!
                    self.offersList = cartArray
                    self.TV.reloadData()
                    
                }
                
            }
        }

    }
    
}
