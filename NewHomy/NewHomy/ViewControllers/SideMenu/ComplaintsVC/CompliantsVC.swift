//
//  CompliantsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/12/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit

class CompliantsVC: UIViewController {

    @IBOutlet weak var complaintsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        complaintsTextView.text = "Complaints and sugestions".localized
        if(L102Language.currentAppleLanguage() == arabicLang){
            complaintsTextView.textAlignment = .right
        }
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
        if(complaintsTextView.text == "Complaints and sugestions".localized || complaintsTextView.text == ""){
            showMessage(sub: "Please, Enter your message first.")
        }else{
            makeComplaint()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CompliantsVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Complaints and sugestions".localized){
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == "" || textView.text == "Complaints and sugestions".localized){
            textView.text = "Complaints and sugestions".localized
            textView.textColor = .lightGray
        }
    }
}

extension CompliantsVC {
    func makeComplaint() {
        
        DispatchQueue.main.async {
            
            let parameters = ["body": self.complaintsTextView.text]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .complaints, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                if error != nil {
                    self.showMessage(sub: error)
                } else {
                    self.showMessage(sub: "Your message has been sent successfully.".localized ,type: .success)
                    appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                }
            }
        }
        
    }

}
