//
//  SettingsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class SettingsVC: UIViewController {
    
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var messagesLabel: UILabel!
    @IBOutlet weak var callsLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var commentsSwitch: UISwitch!
    @IBOutlet weak var messageSwitch: UISwitch!
    @IBOutlet weak var callsSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(User.shared.receive_calls == "true"){
            callsSwitch.isOn = true
            callsLabel.text = "On".localized
        }else{
            callsSwitch.isOn = false
            callsLabel.text = "Off".localized
        }
        
        if(User.shared.receive_comments == "true"){
            commentsSwitch.isOn = true
            commentsLabel.text = "On".localized
        }else{
            commentsSwitch.isOn = false
            commentsLabel.text = "Off".localized
        }
        
        if(User.shared.receive_messages == "true"){
            messageSwitch.isOn = true
            messagesLabel.text = "On".localized
        }else{
            messageSwitch.isOn = false
            messagesLabel.text = "Off".localized
        }
        
        if(User.shared.receive_notification == "true"){
            notificationSwitch.isOn = true
            notificationLabel.text = "On".localized
        }else{
            notificationSwitch.isOn = false
            notificationLabel.text = "Off".localized
        }
        
    }
    
    @IBAction func changeLangButtonClicked(_ sender: UIButton) {
        self.changeLanguage(storyBoard: storyBoardName.sideMenu.rawValue, vcId: .tabBarNav)
        showMessage(title: "Reopen the application", sub: "Please, reopen the application to work properly after changing the language.", type: .warning, layout: .centeredView)
    }
    
    @IBAction func customeBackClicked(_ sender: UIBarButtonItem) {
        updateProfile()
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if(sender.tag == 11){
            //Notification
            if(sender.isOn){
                notificationLabel.text = "On".localized
            }else{
                notificationLabel.text = "Off".localized
            }
        }else if(sender.tag == 12){
            // Comments
            if(sender.isOn){
                commentsLabel.text = "On".localized
            }else{
                commentsLabel.text = "Off".localized
            }
        }else if(sender.tag == 13){
            // messages
            if(sender.isOn){
                messagesLabel.text = "On".localized
            }else{
                messagesLabel.text = "Off".localized
            }
        }else if(sender.tag == 14){
            // calls
            if(sender.isOn){
                callsLabel.text = "On".localized
            }else{
                callsLabel.text = "Off".localized
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SettingsVC {
    func updateProfile() {
        
        DispatchQueue.main.async {
            
            let manager = Manager()
            
            let parameters = ["receive_notification": self.notificationSwitch.isOn.description,
                              "receive_messages": self.messageSwitch.isOn.description,
                              "receive_comments": self.commentsSwitch.isOn.description,
                              "receive_calls": self.callsSwitch.isOn.description]

            
            manager.perform(methodType: .post, serviceName: .profile, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let user = Mapper<User>().map(JSONObject: json) {
                        
                        User.shared.fillUserModel(model: user)
                                                
                        User.shared.saveData()
                                                
                    }
                    
                    appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                                        
                }
                
            }
        }
        
    }

}
