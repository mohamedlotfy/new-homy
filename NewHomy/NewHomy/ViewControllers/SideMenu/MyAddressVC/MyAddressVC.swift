//
//  MyAddressVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class MyAddressVC: UIViewController {
    
    @IBOutlet weak var TV: UITableView!
    
    var addressList = [Address]()
    
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getAddresses()
    }
    
    @IBAction func addNewAddressButtonClicked(_ sender: UIButton) {
        
        let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "ChooseLocation") as! ChooseLocationVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}

extension MyAddressVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddressTVCell
        
        let address = addressList[indexPath.row]
        
        cell.addressLabel.text = address.description
        
        if(selectedIndex == indexPath.row){
            cell.selectionImageView.image = #imageLiteral(resourceName: "circle")
        }else{
            cell.selectionImageView.image = #imageLiteral(resourceName: "Group 1260")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.TV.reloadData()
        selectAddress(forItemID: addressList[indexPath.row].id)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}

extension MyAddressVC{
    func getAddresses() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .address, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    let cartArray = Mapper<Address>().mapArray(JSONObject: json)!
                    self.addressList = cartArray
                    var index = 0
                    for address in self.addressList{
                        if(address.is_default == "true"){
                            self.selectedIndex = index
                        }
                        index = index + 1
                    }
                    self.TV.reloadData()
                    
                }
                
            }
        }
    }
    
    func selectAddress(forItemID: Int) {
        
        DispatchQueue.main.async {
           
            
            let manager = Manager()
            manager.perform(methodType: .get,arguments: "\(forItemID)",serviceName: .address, parameters: nil) { (json, error) -> Void in
                
                if let err = error {
                    self.showMessage(sub: err)
                }else{

                }
            }
        }
    }

}

