//
//  CancelOrderPopUpVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

protocol CancelPopUpDelegate {
    func cancel(for itemIndex: Int)
}


import UIKit
import BottomPopup

class CancelOrderPopUpVC: BottomPopupViewController, UINavigationControllerDelegate  {
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var delegate: CancelPopUpDelegate!
    var itemIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func confirmButtonClicked(_ sender: UIButton) {
        delegate.cancel(for: itemIndex ?? 0)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(250)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.2
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
}
