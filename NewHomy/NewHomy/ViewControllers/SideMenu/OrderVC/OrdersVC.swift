//
//  OrdersVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper
import BottomPopup

class OrdersVC: UIViewController, BottomPopupDelegate {
    
    @IBOutlet weak var TV: UITableView!
    
    
    var cartList = [Order]()
    var selectedCategory = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.TV.isHidden = true
        getOrders()
    }
    
   
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if(segue.identifier == "details"){
            let vc = segue.destination as! OrderDetailsVC
            if let order = sender as? Order{
                vc.productList = order.products
                vc.orderCodeValue = order.code
            }
        }
     }
     
    
}

extension OrdersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cartList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartTVCell
        
        let product = cartList[indexPath.item]
        
        cell.costLabel.text = product.status
        cell.serviceNameLabel.text = product.provider + "- #\(product.code)"
        
//        if(product.images.count > 0){
//            if let imageLink = URL(string: product.images[0].image){
//                cell.serviceImageView.kf.setImage(with: imageLink)
//            }
//        }

        cell.deleteButton.tag = 100 + indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func deleteButtonClicked(_ sender: UIButton){
        if(sender.tag >= 100){
            guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "CancelPopUp") as? CancelOrderPopUpVC else { return }
            popupVC.height = 280
            popupVC.topCornerRadius = 35
            popupVC.presentDuration = 1
            popupVC.dismissDuration = 0.3
            popupVC.itemIndex = sender.tag - 100
            popupVC.popupDelegate = self
            popupVC.delegate = self
            self.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(cartList[indexPath.row].type == "material"){
            performSegue(withIdentifier: "details", sender: cartList[indexPath.row])
        }
    }
}

extension OrdersVC{
    func getOrders() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .order, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    let cartArray = Mapper<Order>().mapArray(JSONObject: json)!
                    self.cartList = cartArray
                    if(self.cartList.count > 0){
                        
                        self.TV.reloadData()
                        self.TV.isHidden = false
                        
                    }else{
                        
                        self.TV.isHidden = true
                        
                    }
                    
                }
                
            }
        }
    }
    
    func deleteCart(forItemID: Int) {
        
        DispatchQueue.main.async {
           
            
            let manager = Manager()
            manager.perform(methodType: .delete,arguments: "\(forItemID)",serviceName: .order, parameters: nil) { (json, error) -> Void in
                
                if let err = error {
                    self.showMessage(sub: err)
                }else{
                    self.TV.reloadData()
                }
            }
        }
    }

}

extension OrdersVC: CancelPopUpDelegate{
    
    func cancel(for itemIndex: Int) {
        self.deleteCart(forItemID: self.cartList[itemIndex].id)
        self.cartList[self.selectedCategory].products.remove(at: itemIndex)
        self.cartList.remove(at: itemIndex)

    }
    
}
