//
//  OrderDetailsVC.swift
//  NewHomy
//
//  Created by haniielmalky on 11/20/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import Cosmos

class OrderDetailsVC: UIViewController {

    @IBOutlet weak var orderCode: UINavigationItem!
    @IBOutlet weak var TV: UITableView!
    
    var productList = [CartProduct]()
    var orderCodeValue = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        orderCode.title = orderCodeValue
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension OrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderDetailsTVCell
        
        let product = productList[indexPath.item]
        
        cell.productNameLabel.text = product.name
        cell.rateView.rating = Double(product.rate)
        cell.rateView.didFinishTouchingCosmos = { rating in
            // call rate function
            self.rate(productId: product.materialId, rate: rating)
        }

        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
}

extension OrderDetailsVC {
    
       func rate(productId: Int, rate: Double){
           
           DispatchQueue.main.async {
               
               
               let parameters = ["product_id": productId,
                                 "rate": rate] as [String : Any]
               
               let manager = Manager()
               manager.perform(methodType: .post, serviceName: .rate, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                   
                   if error != nil {
                       self.showMessage(sub: error)
                   } else {
                   }
               }
           }
       }
}
