//
//  OrderDetailsTVCell.swift
//  NewHomy
//
//  Created by haniielmalky on 11/20/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import Cosmos

class OrderDetailsTVCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
