//
//  ProfileVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/13/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class ProfileVC: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var birthdateTF: UITextField!
    @IBOutlet weak var nameEditButton: UIButton!
    @IBOutlet weak var emailEditButton: UIButton!
    @IBOutlet weak var phoneEditButton: UIButton!
    @IBOutlet weak var birthdateEditButton: UIButton!
    
    var birthdatePickerView = UIDatePicker()
    var imagePicker = PickImageVC()
    var selectedImage: UIImage?
    
    var parameters = ["mobile": User.shared.mobile!]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if(User.shared.profileImage != ""){
            if let imageLink = URL(string: User.shared.profileImage!){
                profileImageView.kf.setImage(with: imageLink)
            }
        }
        
        if(User.shared.username != ""){
            nameTF.text = User.shared.username!
        }
        
        if(User.shared.email != ""){
            emailTF.text = User.shared.email!
        }
        
        if(User.shared.birthday != ""){
            birthdateTF.text = User.shared.birthday!
        }
        
        phoneTF.text = User.shared.mobile!
        
        birthdateTF.inputView = birthdatePickerView
        birthdatePickerView.datePickerMode = .date
        birthdatePickerView.maximumDate = Date()
        imagePicker.delegate = self
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.transparentNavBar()
    }
    
    @IBAction func editButtonClicked(_ sender: UIButton) {
        if(sender == nameEditButton){
            nameEditButton.isHidden = true
            nameTF.isUserInteractionEnabled = true
            nameTF.becomeFirstResponder()
        }else if(sender == emailEditButton){
            emailEditButton.isHidden = true
            emailTF.isUserInteractionEnabled = true
            emailTF.becomeFirstResponder()
        }else if(sender == phoneEditButton){
            phoneEditButton.isHidden = true
            phoneTF.isUserInteractionEnabled = true
            phoneTF.becomeFirstResponder()
        }else if(sender == birthdateEditButton){
            birthdateEditButton.isHidden = true
            birthdateTF.isUserInteractionEnabled = true
            birthdateTF.becomeFirstResponder()
        }
        
    }
    
    @IBAction func textFieldDidEndEdit(_ sender: UITextField) {
        if(sender == nameTF){
            nameEditButton.isHidden = false
            nameTF.isUserInteractionEnabled = false
            nameTF.resignFirstResponder()
            parameters["username"] = nameTF.text!
        }else if(sender == emailTF){
            emailEditButton.isHidden = false
            emailTF.isUserInteractionEnabled = false
            emailTF.resignFirstResponder()
            parameters["email"] = nameTF.text!
        }else if(sender == phoneTF){
            phoneEditButton.isHidden = false
            phoneTF.isUserInteractionEnabled = false
            phoneTF.resignFirstResponder()
        }else if(sender == birthdateTF){
            birthdateEditButton.isHidden = false
            birthdateTF.isUserInteractionEnabled = false
            birthdateTF.resignFirstResponder()
            birthdateTF.text = birthdatePickerView.date.dateString
            parameters["birthday"] = birthdatePickerView.date.formatToSend
            
        }
    }
    
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        updateProfile()
    }
    
    @IBAction func profileImageButtonClicked(_ sender: UIButton) {
        imagePicker.pick(vc: self)
    }
    
}

extension ProfileVC: PickerImageDelegate{
    func didPickedImage(_ image: UIImage) {
        profileImageView.image = image
        updateImage(profileImage: image)
    }
}

extension ProfileVC {
    
    func updateProfile() {
        
        DispatchQueue.main.async {
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .profile, parameters: self.parameters as [String : AnyObject]) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let user = Mapper<User>().map(JSONObject: json) {
                        
                        User.shared.fillUserModel(model: user)
                                                
                        User.shared.saveData()
                        
                        
                        
                    }
                    
                    appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                    
                }
                
            }
        }
        
    }
    
    
    func updateImage(profileImage : UIImage) {
        DispatchQueue.main.async {
            let manager = Manager()
            self.startAnimating()
            manager.uploadImage(serviceName: .profile, profileImage: profileImage, parameters: nil, progress: { (percent) in
                
            }, completionHandler: { (JSON, String) in
                self.stopAnimating()
                
                if String != nil {
                    self.showMessage(sub: String!)
                }else{
                    if let user = Mapper<User>().map(JSONObject: JSON) {
                        
                        User.shared.fillUserModel(model: user)
                                                
                        User.shared.saveData()
                        
                        
                    }
                    
                }
            })
        }
    }
}
