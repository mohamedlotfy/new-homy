//
//  PaymentVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/21/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentVC: UIViewController {
    
    @IBOutlet weak var cashImageView: UIImageView!
    @IBOutlet weak var creditImageView: UIImageView!
    
    var isCash = true
    var initialSetupViewController: PTFWInitialSetupViewController!
    var timeString: String?
    var dateString: String?
    var carts = [Int]()
    var price : Float = 0
    var productPrices = [(Int, Float)]()
    var cartList : Cart?
    var cartsString = ""
    var cartStringsArray = [(Int, String)]()
    var newAddress = false
    var addressId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCart()
        cartsString = ""
        var first = true
        for cart in self.carts{
            if(first){
                cartsString = "\(cart)"
                first = false
            }else{
                cartsString = cartsString + ",\(cart)"
            }
        }
        if(newAddress){
            getAddresses()
        }else{
            addressId = User.shared.address_id
        }
        
    }
    
    @IBAction func cashButtonClicked(_ sender: UIButton) {
        if(!isCash){
            creditImageView.image = #imageLiteral(resourceName: "Group 1260")
            cashImageView.image = #imageLiteral(resourceName: "Group 8009")
            isCash = true
        }
    }
    
    @IBAction func creditButtonClicked(_ sender: UIButton) {
        if(isCash){
            creditImageView.image = #imageLiteral(resourceName: "Group 8009")
            cashImageView.image = #imageLiteral(resourceName: "Group 1260")
            isCash = false
        }
    }
    
    @IBAction func chooseButtonClicked(_ sender: UIButton) {
        if(isCash){
            makeOrder(paymentId: 1)
        }else{
            getCart()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PaymentVC{
    
    func setUpPayTaps(){
        let bundle = Bundle(url: Bundle.main.url(forResource: "Resources", withExtension: "bundle")!)
        self.initialSetupViewController = PTFWInitialSetupViewController.init(
            bundle: bundle,
            andWithViewFrame: self.view.frame,
            andWithAmount: self.price,
            andWithCustomerTitle: "Homy Order Payment",
            andWithCurrencyCode: "SAR",
            andWithTaxAmount: 0.0,
            andWithSDKLanguage: "en",
            andWithShippingAddress: "شارع الخزان، ابراج الخالدية، الدور ٢١- مؤسسة زوايا الجزيرة اتقنية المعلومات",
            andWithShippingCity: "الرياض",
            andWithShippingCountry: "SAU",
            andWithShippingState: "حي الغوطه",
            andWithShippingZIPCode: "966",
            andWithBillingAddress: "شارع الخزان، ابراج الخالدية، الدور ٢١- مؤسسة زوايا الجزيرة اتقنية المعلومات",
            andWithBillingCity: "الرياض",
            andWithBillingCountry: "SAU",
            andWithBillingState: "حي الغوطه",
            andWithBillingZIPCode: "966",
            andWithOrderID: self.cartsString,
            andWithPhoneNumber: "00" + User.shared.mobile!,
            andWithCustomerEmail: "hani@za.com.sa",
            andIsTokenization: true,
            andIsPreAuth: false,
            andWithMerchantEmail: "paytabs@za.com.sa",
            andWithMerchantSecretKey: "aHMHFtCWZL9650Ba5CtE4UmXTyn5xFEHnrp3V1ZUM80S9C2fewTjrghkOEqlk53HwsKrtKmgTn83ZbieIbx9rOugtUBNZFuw7fYB",
            andWithAssigneeCode: "SDK",
            andWithThemeColor : #colorLiteral(red: 0.831, green: 0.349, blue: 0.298, alpha: 1.0),
            andIsThemeColorLight: false)
        
        
        self.initialSetupViewController.didReceiveBackButtonCallback = {
            
        }
        
        self.initialSetupViewController.didStartPreparePaymentPage = {
            // Start Prepare Payment Page
            // Show loading indicator
        }
        self.initialSetupViewController.didFinishPreparePaymentPage = {
            // Finish Prepare Payment Page
            // Stop loading indicator
        }
        
        self.initialSetupViewController.didReceiveFinishTransactionCallback = {(responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState) in
            
            if(responseCode == 100){
                self.makeOrder(paymentId: 2)
            }else{
                self.showMessage(sub: result)
            }
        }
        
        self.view.addSubview(initialSetupViewController.view)
        self.addChild(initialSetupViewController)
        
        initialSetupViewController.didMove(toParent: self)
    }
    
}

extension PaymentVC {
    func makeOrder(paymentId: Int) {
        for item in cartList!.products{
            for product in carts{
                if(product == item.id){
                    if(cartStringsArray.count == 0){
                        cartStringsArray.append((item.provider_id, "\(product)"))
                    }else{
                        var index = 0
                        var check = 0
                        for cartString in cartStringsArray {
                            if(cartString.0 == item.provider_id){
                                cartStringsArray[index].1 = cartString.1 + ",\(product)"
                                check = 1
                            }
                            index = index + 1
                        }
                        if(check == 0){
                            cartStringsArray.append((item.provider_id, "\(product)"))
                        }
                    }
                }
            }
        }
        
        for cartString in cartStringsArray{
            makeSeprateOrders(cartString: cartString.1, providerId: cartString.0, paymetnId: paymentId)
        }
        
    }
    
    
    func makeSeprateOrders(cartString: String, providerId: Int, paymetnId: Int){
        
        DispatchQueue.main.async {
            
            
            let parameters = ["time": self.timeString,
                              "day": self.dateString,
                              "address_id": self.addressId,
                              "carts": cartString,
                              "provider_id":providerId,
                              "payment_id":paymetnId,
                              "type":"material"] as [String : Any]
            
            let manager = Manager()
            manager.perform(methodType: .post, serviceName: .order, parameters: parameters as [String : AnyObject]) { (json, error) -> Void in
                
                if error != nil {
                    self.showMessage(sub: error)
                } else {
                    self.showMessage(sub: "Your order has been placed successfully.".localized ,type: .success)
                    appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
                }
            }
        }
    }
    
    func getCart() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .cart, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    if let cart = Mapper<Cart>().map(JSONObject: json) {
                        self.cartList = cart
                        if(!self.isCash){
                            self.getTotal()
                        }
                    }
                }
                
            }
        }
    }
    
    func getTotal(){
        for product in cartList!.products{
            for cart in carts{
                if(cart == product.id){
                    price = price + (Float(product.price) * Float(product.count)!)
                }
            }
            
        }
        
        setUpPayTaps()
    }
    
    func getAddresses() {
        
        DispatchQueue.main.async {
            
            self.startAnimating()
            
            
            let manager = Manager()
            manager.perform(methodType: .get, serviceName: .address, parameters: nil) { (json, error) -> Void in
                
                self.stopAnimating()
                
                if let err = error {
                    self.showMessage(sub: err)
                } else {
                    
                    var cartArray = Mapper<Address>().mapArray(JSONObject: json)!
                    cartArray = cartArray.sorted(by: { $0.id > $1.id })
                    
                    if(cartArray.count > 0){
                        self.addressId = "\(cartArray[0].id)"
                    }
                }
                
            }
        }
    }
    
}
