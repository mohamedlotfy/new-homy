//
//  BaseVC.swift
//  NewHomy
//
//  Created by haniielmalky on 10/22/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import UIKit
import SWRevealViewController

class BaseVC: UIViewController, SWRevealViewControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var sideMenuButton: UIBarButtonItem!

    
    let dimView = UIView()
    let screenSize = UIScreen.main.bounds

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        revealViewController().delegate = self
        createDimView()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        revealViewController().delegate = self
        
        if revealViewController() != nil {
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            self.revealViewController().panGestureRecognizer().isEnabled = true
        }
        
        integrateSideMenu()

    }
    
    
    func createDimView(){
        dimView.frame = CGRect.init(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        dimView.backgroundColor = .clear
        self.view.addSubview(dimView)
        dimView.isHidden = true
    }

    
    func integrateSideMenu() {
        if revealViewController() != nil {
            
            if L102Language.currentAppleLanguage() == englishLang {
                if(menuButton != nil){
                    menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                }
                if(sideMenuButton != nil){
                    sideMenuButton.action = #selector(SWRevealViewController.revealToggle(_:))
                }
                revealViewController().rearViewRevealWidth = self.view.frame.size.width / 1.3
                revealViewController().rightViewRevealWidth = 0
                revealViewController().rightViewRevealOverdraw = 0
                revealViewController().rearViewRevealOverdraw = 0
            }
            else{
                if(sideMenuButton != nil){
                    sideMenuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
                }
                if(menuButton != nil){
                    menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
                }
                revealViewController().rightViewRevealWidth = self.view.frame.size.width / 1.3
                revealViewController().rightViewRevealOverdraw = 0
                revealViewController().rearViewRevealWidth = 0
                revealViewController().rearViewRevealOverdraw = 0
            }
        }
    }
    
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if(position == .left) {
            dimView.isHidden = true
        }
        else {
            dimView.isHidden = false
            self.view.endEditing(true)
        }
    }

}
