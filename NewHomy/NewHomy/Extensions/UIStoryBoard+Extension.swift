//
//  6.swift
//  TYOUT
//
//  Created by Mohamed Lotfy on 9/25/18.
//  Copyright © 2018 Gra7. All rights reserved.
//

import Foundation
import UIKit

enum storyBoardName: String {
    case authentication = "Authentication"
    case home = "Home"
    case sideMenu = "SideMenu"

}

enum storyBoardVCIDs: String {
    
    case tabBarNav = "tabBarNavigation"
    case ChooseServiceVC = "ChooseServiceVC"
    case profile = "profile"
    case complaints = "Comp"
    case orders = "Orders"
    case settings = "Settings"
    case login = "login"
    case address = "Addresss"
    case chooseServiceNav = "chooseServiceNav"
}



extension UIStoryboard {
    class func instantiateInitialViewController(_ board: storyBoardName) -> UIViewController {
        let story = UIStoryboard(name: board.rawValue, bundle: nil)
        return story.instantiateInitialViewController()!
    }
}
