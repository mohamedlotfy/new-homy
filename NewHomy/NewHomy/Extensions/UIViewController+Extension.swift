//
//  5.swift
//  TYOUT
//
//  Created by Mohamed Lotfy on 9/25/18.
//  Copyright © 2018 Gra7. All rights reserved.
//

import UIKit
import Kingfisher
import AudioToolbox
//import StatusAlert
import LXStatusAlert
import UIView_Shake
import SwiftEntryKit
import SwiftMessages
import NVActivityIndicatorView
import SWRevealViewController

extension UIViewController : UIGestureRecognizerDelegate, NVActivityIndicatorViewable{
   
    @IBAction func menuBackClicked(_ sender: Any) {
        appDelegate.setRoot(storyBoard: .sideMenu, vc: .tabBarNav)
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
           _ = self.navigationController?.popViewController(animated: true)
    }
       
    @IBAction func searchButtonClicked(_ sender: Any){
        
        let storyboard = UIStoryboard.init(name: "SideMenu", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    @IBAction func dismissClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func startAnimating(text:String?){
        
            let presentingIndicatorTypes = {
                 return NVActivityIndicatorType.allCases.filter { $0 != .blank }
             }()
        
        let size = CGSize(width: 30, height: 30)
//               let selectedIndicatorIndex = sender.tag
               let indicatorType = presentingIndicatorTypes[5]

        startAnimating(size, message: text ?? "", type: indicatorType, color: #colorLiteral(red: 0.9882352941, green: 0.3333333333, blue: 0.2941176471, alpha: 1),textColor: #colorLiteral(red: 0.9882352941, green: 0.3333333333, blue: 0.2941176471, alpha: 1),  fadeInAnimation: nil)

//               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
//                   NVActivityIndicatorPresenter.sharedInstance.setMessage("Authenticating...")
//               }

//               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//               }

    }
    
    func stopAnimating()  {
        self.stopAnimating(nil)
    }
   
//    @IBAction func notificationClicked(_ sender: Any) {
//        let storyboard = UIStoryboard.init(name: "UserLanding", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
//        self.navigationController?.pushViewController(vc, animated: true)
//        
//    }
//    @IBAction func messagesClicked(_ sender: Any) {
//        let storyboard = UIStoryboard.init(name: "GuidLanding", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier: "MessagesVC") as! MessagesVC
//        self.navigationController?.pushViewController(vc, animated: true)
//        
//    }
    func showMessage(title: String? = nil, sub: String?, type: Theme = .warning, layout: MessageView.Layout = .statusLine) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: layout)
        
        // Theme message elements with the warning style.
        view.configureTheme(type)
        view.button?.isHidden = true
        

        if type == .error {
            self.view.shake()
            Vibration.error.vibrate()
        }
        // Add a drop shadow.
        //        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        
        view.configureContent(title: title ?? "", body: sub ?? "", iconText: "")
        
        // Show the message.
        SwiftMessages.show(view: view)
    }
    func showpopView(myView: UIView,hieght: CGFloat) {
        var attributes = EKAttributes()
        attributes = .centerFloat
        attributes.hapticFeedbackType = .success
        attributes.displayDuration = .infinity
        attributes.screenBackground = .visualEffect(style: .dark)
        attributes.entryBackground = .clear
        attributes.screenInteraction = .dismiss
        attributes.entryInteraction = .absorbTouches
        
        attributes.entranceAnimation = .init(translate: .init(duration: 0.6, spring: .init(damping: 0.9, initialVelocity: 0)),
                                             scale: .init(from: 0.8, to: 1, duration: 0.6, spring: .init(damping: 0.8, initialVelocity: 0)),
                                             fade: .init(from: 0.7, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.5),
                                         scale: .init(from: 1, to: 0.8, duration: 0.5),
                                         fade: .init(from: 1, to: 0, duration: 0.5))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3),
                                                            scale: .init(from: 1, to: 0.8, duration: 0.3)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 6))
        attributes.positionConstraints.verticalOffset = 10
        attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .constant(value: hieght))
        attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
        attributes.statusBar = .inferred
        SwiftEntryKit.display(entry: myView, using: attributes)
    }
    
    
    func addPopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func alert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    func isNotEmptyString(text: String, withAlertMessage message: String) -> Bool{
        if text == ""{
            //            showAlertWithTitle(title: "Empty Field".localized, message: message, type: .error);
            showMessage(title: "Empty Field".localized, sub: message, type: .error, layout: .statusLine)
            //            Vibration.error.vibrate()
            
            return false
        }
        else{
            return true
        }
    }
    func showStatus(image: UIImage , message: String){
//        let statusAlert = StatusAlert()
//        statusAlert.image = image
//        statusAlert.title = title
//        statusAlert.message = message
////        statusAlert.canBePickedOrDismissed = isUserInteractionAllowed
//        statusAlert.canBePickedOrDismissed = true
//        statusAlert.showInKeyWindow()
        let statusAlert = LXStatusAlert(image: image, title:message, duration: 3.0)
        statusAlert.show()

    }
    func alertSkipLogin(){
        let alert = UIAlertController.init(title: "Warning".localized , message: "please login first".localized ,  preferredStyle: .alert)
        alert.view.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        
        
        let cancelAction = UIAlertAction.init(title: "Ok".localized, style: .cancel, handler: { (nil) in
            
//            appDelegate.setRoot(storyBoard: .authentication, vc: .splash)
            
            
        })
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func transparentNavBar() {
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           self.navigationController?.navigationBar.shadowImage = UIImage()
           self.navigationController?.navigationBar.isTranslucent = true
           self.navigationController?.view.backgroundColor = .clear
       }
    
    func setShadow(view : UIView , width : Int , height: Int , shadowRadius: CGFloat , shadowOpacity: Float , shadowColor: CGColor){
        // to make the shadow with rounded corners and offset shadow form the bottom
        view.layer.shadowColor = shadowColor
        view.layer.shadowOffset = CGSize(width: width, height: height)
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOpacity = shadowOpacity
        view.clipsToBounds = true
        view.layer.masksToBounds = false
    }
    enum Vibration {
        case error
        case success
        case warning
        case light
        case medium
        case heavy
        case selection
        case oldSchool
        func vibrate() {
            
            switch self {
            case .error:
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.error)
                
            case .success:
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.success)
                
            case .warning:
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.warning)
                
            case .light:
                let generator = UIImpactFeedbackGenerator(style: .light)
                generator.impactOccurred()
                
            case .medium:
                let generator = UIImpactFeedbackGenerator(style: .medium)
                generator.impactOccurred()
                
            case .heavy:
                let generator = UIImpactFeedbackGenerator(style: .heavy)
                generator.impactOccurred()
                
            case .selection:
                let generator = UISelectionFeedbackGenerator()
                generator.selectionChanged()
                
            case .oldSchool:
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            
        }
        
    }
    func changeLanguage(storyBoard: String = "SideMenu", vcId: storyBoardVCIDs) {
        let transition: UIView.AnimationOptions = .transitionCrossDissolve
        
        if L102Language.currentAppleLanguage() == englishLang {
            L102Language.setAppleLAnguageTo(lang: arabicLang)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            L102Language.setAppleLAnguageTo(lang: englishLang)
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        let storyBoard: UIStoryboard = UIStoryboard.init(name: storyBoard, bundle: nil)
        
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = storyBoard.instantiateViewController(withIdentifier: vcId.rawValue)
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
    }
//    func changeLanguage() {
//        let transition: UIView.AnimationOptions = .transitionCrossDissolve
//
//        if L102Language.currentAppleLanguage() == englishLang {
//            L102Language.setAppleLAnguageTo(lang: arabicLang)
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//        } else {
//            L102Language.setAppleLAnguageTo(lang: englishLang)
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//        }
//
//        let storyBoard: UIStoryboard = UIStoryboard.init(name: "Landing", bundle: nil)
//
//
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        rootviewcontroller.rootViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController")
//        let mainwindow = (UIApplication.shared.delegate?.window!)!
//        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
//        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
//        }) { (finished) -> Void in
//
//        }
//    }
    
    func performSegueTo(storyBoard: storyBoardName, vc: storyBoardVCIDs) {
        let sb = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        let vcNew = sb.instantiateViewController(withIdentifier: vc.rawValue)
        self.navigationController?.pushViewController(vcNew, animated: true)
//        show(vcNew, sender: self)
        
    }
    
    
    func setMenuFrontView(storyBoard: storyBoardName, vc: storyBoardVCIDs) {
        let sb = UIStoryboard(name: storyBoard.rawValue, bundle: nil)
        let vcNew = sb.instantiateViewController(withIdentifier: vc.rawValue) as! UINavigationController
        self.revealViewController().setFront(vcNew, animated: true)
        self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
    }

    
    
}
extension UIScreen {
    var minEdge: CGFloat {
        return UIScreen.main.bounds.minEdge
    }
}
extension CGRect {
    var minEdge: CGFloat {
        return min(width, height)
    }
}
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
