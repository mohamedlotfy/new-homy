//
//  Cart.swift
//  NewHomy
//
//  Created by haniielmalky on 10/16/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Cart: Mappable {
    
    var providerName : String = ""
    var providerId : Int = -1
    var deliveryFax : String = ""
    var total : Int = -1
    var products = [CartProduct]()
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        providerName    <- map["provider_name"]
        providerId      <- map["provider_id"]
        deliveryFax     <- map["delivery_fax"]
        total           <- map["total"]
        products        <- map["carts"]
    }
    
    
}


struct CartProduct: Mappable {
    
  
    var id = -1
    var name = ""
    var materialId = -1
    var provider_id = -1
    var rate = -1
    var price = -1
    var count = ""
    var type = ""
    var images = [Image]()
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        materialId  <- map["material_id"]
        provider_id <- map["provider_id"]
        rate        <- map["rate"]
        price       <- map["price"]
        count       <- map["count"]
        type        <- map["type"]
        images      <- map["image"]
        
    }
    
    
}



