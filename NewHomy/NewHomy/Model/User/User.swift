//
//  User.swift
//  WomenAccessories
//
//  Created by Mohamed Lotfy on 12/6/18.
//  Copyright © 2018 Gra7. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, Mappable, Codable, NSCoding {
    
    static let shared = User()
    
    var id : Int?
    var username : String?
    var email: String?
    var mobile: String?
    var birthday: String?
    var receive_notification: String?
    var receive_messages: String?
    var receive_comments: String?
    var receive_calls: String?
    var profileImage: String?
    var type: String?
    var address_id: String?
    var address_lat: String?
    var address_lng: String?
    var device_token: String?
    var token: String?
    var status: String?
    
    
    
    override init() {
        super.init()
    }
    
    
    required init?(map: Map) {}
    
    
    func mapping(map: Map) {
                
        self.id                     <- map["id"]
        self.username               <- map["username"]
        self.email                  <- map["email"]
        self.mobile                 <- map["mobile"]
        self.birthday               <- map["birthday"]
        self.type                   <- map["type"]
        self.receive_notification   <- map["receive_notification"]
        self.receive_messages       <- map["receive_messages"]
        self.receive_comments       <- map["receive_comments"]
        self.receive_calls          <- map["receive_calls"]
        self.profileImage           <- map["image"]
        self.address_id             <- map["address_id"]
        self.address_lat            <- map["address_lat"]
        self.address_lng            <- map["address_lng"]
        self.device_token           <- map["device_token"]
        self.status                 <- map["status"]
        self.token                  <- map["token"]
                
    }
    
    
    func fillUserModel(model: User) {
        
        self.id                     = model.id
        self.username               = model.username
        self.email                  = model.email
        self.mobile                 = model.mobile
        self.birthday               = model.birthday
        self.type                   = model.type
        self.receive_notification   = model.receive_notification
        self.receive_messages       = model.receive_messages
        self.receive_comments       = model.receive_comments
        self.receive_calls          = model.receive_calls
        self.profileImage           = model.profileImage
        self.address_lng            = model.address_lng
        self.address_lat            = model.address_lat
        self.address_id             = model.address_id
        self.device_token           = model.device_token
        self.status                 = model.status
        self.token                  = model.token
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        
        self.init()
        
        User.shared.profileImage            = aDecoder.decodeObject(forKey: "profile_Image") as? String
        User.shared.id                      = aDecoder.decodeObject(forKey: "id") as? Int
        User.shared.email                   = aDecoder.decodeObject(forKey: "email") as? String
        User.shared.username                = aDecoder.decodeObject(forKey: "username") as? String
        User.shared.mobile                  = aDecoder.decodeObject(forKey: "mobile") as? String
        User.shared.birthday                = aDecoder.decodeObject(forKey: "birthday") as? String
        User.shared.type                    = aDecoder.decodeObject(forKey: "type") as? String
        User.shared.receive_notification    = aDecoder.decodeObject(forKey: "receive_notification") as? String
        User.shared.receive_comments        = aDecoder.decodeObject(forKey: "receive_comments") as? String
        User.shared.receive_messages        = aDecoder.decodeObject(forKey: "receive_messages") as? String
        User.shared.receive_calls           = aDecoder.decodeObject(forKey: "receive_calls") as? String
        User.shared.address_id              = aDecoder.decodeObject(forKey: "address_id") as? String
        User.shared.address_lat             = aDecoder.decodeObject(forKey: "address_lat") as? String
        User.shared.address_lng             = aDecoder.decodeObject(forKey: "address_lng") as? String
        User.shared.device_token            = aDecoder.decodeObject(forKey: "device_token") as? String
        User.shared.status                  = aDecoder.decodeObject(forKey: "status") as? String
        User.shared.token                   = aDecoder.decodeObject(forKey: "token") as? String
        
    }
    
    
    func encode(with coder: NSCoder) {
        
        coder.encode(id, forKey: "id")
        coder.encode(email, forKey: "email")
        coder.encode(username, forKey: "username")
        coder.encode(mobile, forKey: "mobile")
        coder.encode(birthday, forKey: "birthday")
        coder.encode(type, forKey: "type")
        coder.encode(receive_notification, forKey: "receive_notification")
        coder.encode(receive_comments, forKey: "receive_comments")
        coder.encode(receive_messages, forKey: "receive_messages")
        coder.encode(receive_calls, forKey: "receive_calls")
        coder.encode(profileImage, forKey: "profile_Image")
        coder.encode(address_id, forKey: "address_id")
        coder.encode(address_lat, forKey: "address_lat")
        coder.encode(address_lng, forKey: "address_lng")
        coder.encode(device_token, forKey: "device_token")
        coder.encode(status, forKey: "status")
        coder.encode(token, forKey: "token")
        
    }
    
    
    func loadData(){
        let userDefaults = UserDefaults.standard
        NSKeyedUnarchiver.unarchiveObject(with: (userDefaults.object(forKey: "User") as! NSData) as Data)
    }
    
    
    func saveData(){
        print("self:\(self)")
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: "User")
    }
    
    
    func isRegistered() -> Bool{
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: "IsRegistered")
    }
    
    
    func setIsRegister(registered: Bool){
        let defaults = UserDefaults.standard
        defaults.set(registered, forKey: "IsRegistered")
    }
    
    func logout() {
        
        self.id                     = nil
        self.username               = nil
        self.email                  = nil
        self.mobile                 = nil
        self.birthday               = nil
        self.type                   = nil
        self.receive_notification   = nil
        self.receive_messages       = nil
        self.receive_comments       = nil
        self.receive_calls          = nil
        self.profileImage           = nil
        self.address_id             = nil
        self.address_lng            = nil
        self.address_lat            = nil
        self.device_token           = nil
        self.status                 = nil
        self.token                  = nil
        
        self.setIsRegister(registered: false)
        
    }
}
