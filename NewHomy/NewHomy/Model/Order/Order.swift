//
//  Order.swift
//  NewHomy
//
//  Created by haniielmalky on 11/20/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Order: Mappable {
    
    var id = -1
    var code = ""
    var total_price = ""
    var status = ""
    var payment_id = -1
    var payment = ""
    var address_id = -1
    var address = ""
    var day = ""
    var time = "25"
    var provider_id = -1
    var provider = ""
    var type = ""
    var products = [CartProduct]()
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        code        <- map["code"]
        total_price <- map["total_price"]
        status      <- map["status"]
        payment_id  <- map["payment_id"]
        payment     <- map["payment"]
        address_id  <- map["address_id"]
        address     <- map["address"]
        day         <- map["day"]
        time        <- map["time"]
        provider_id <- map["provider_id"]
        provider    <- map["provider"]
        type        <- map["type"]
        products    <- map["carts"]
        
    }

}
