//
//  Company.swift
//  NewHomy
//
//  Created by haniielmalky on 10/15/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Company: Mappable {
    
    var id: Int = -1
    var name: String = ""
    var lat: String = ""
    var lng: String = ""
    var address: String = ""
    var image: String = ""
    var type: String = ""
    var tawkel: String = ""
    var mobile: String = ""
    var email: String = ""
    var offer: String = ""

    
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        lat         <- map["lat"]
        lng         <- map["lng"]
        address     <- map["address"]
        image       <- map["image"]
        type        <- map["type"]
        tawkel      <- map["tawkel"]
        mobile      <- map["mobile"]
        email       <- map["email"]
        offer       <- map["offer"]
        
    }
    
    
}
