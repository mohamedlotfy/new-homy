//
//  CompanyDetails.swift
//  NewHomy
//
//  Created by haniielmalky on 10/15/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct CompanyDetails: Mappable {
    
    var id: Int = -1
    var name: String = ""
    var lat: String = ""
    var lng: String = ""
    var address: String = ""
    var products = [CompanyProduct]()

    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        lat         <- map["lat"]
        lng         <- map["lng"]
        address     <- map["address"]
        products    <- map["products"]
        
    }
    
    
}


struct CompanyCategory: Mappable {
    
    var id: Int = -1
    var name: String = ""
    var products = [CompanyProduct]()

    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        products    <- map["products"]

    }
    
    
}


struct CompanyProduct: Mappable {
    
    var id: Int = -1
    var name: String = ""
    var description: String = ""
    var images = [Image]()
    var price: String = ""
    var companyName: String = ""
    var companyId: Int = -1
    var rate : Int = -1
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        description <- map["description"]
        images      <- map["images"]
        price       <- map["price"]
        companyName <- map["company_name"]
        companyId   <- map["company_id"]
        rate        <- map["rate"]
    }
    
    
}



