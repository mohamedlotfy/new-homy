//
//  Address.swift
//  NewHomy
//
//  Created by haniielmalky on 10/17/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Address: Mappable {
    
    var id: Int = -1
    var lat: String = ""
    var lng: String = ""
    var title: String = ""
    var description: String = ""
    var is_default: String = ""
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        lat         <- map["lat"]
        lng         <- map["lng"]
        title       <- map["title"]
        description <- map["description"]
        is_default  <- map["is_default"]
    }
    
    
}

