//
//  Category.swift
//  NewHomy
//
//  Created by haniielmalky on 10/15/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Category: Mappable {
    
    var id : Int = -1
    var name: String = ""
    var image: String = ""
    var description: String = ""
    var type: String = ""
    var images = [Image]()
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        image       <- map["image"]
        description <- map["description"]
        type        <- map["type"]
        images      <- map["images"]
        
    }
    
    
}


struct Image: Mappable {
      
      var id : Int = -1
      var image: String = ""
      
      init?(map: Map) {
          
      }
      
      
      mutating func mapping(map: Map) {
          
          id      <- map["id"]
          image   <- map["image"]
      }
      
}
