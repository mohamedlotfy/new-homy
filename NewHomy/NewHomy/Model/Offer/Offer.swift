//
//  Offer.swift
//  NewHomy
//
//  Created by haniielmalky on 10/24/19.
//  Copyright © 2019 com.Exception. All rights reserved.
//

import Foundation
import ObjectMapper

struct Offer: Mappable {
    
    var id: Int = -1
    var name: String = ""
    var description: String =  ""
    var discount: String = ""
    var imageLink: String = ""
    var companyId : Int = -1
    var companyName: String = ""
    var service : CompanyProduct?
    
    init?(map: Map) {
        
    }
    
    
    mutating func mapping(map: Map) {
        
        id          <- map["id"]
        name        <- map["name"]
        description <- map["description"]
        discount    <- map["discount"]
        imageLink   <- map["image"]
        companyId   <- map["company_id"]
        companyName <- map["company_name"]
        service     <- map["service"]
    }
    
    
}
