//
//  Manager.swift
//  Levo User Application
//
//  Created by haniielmalky on 12/10/18.
//  Copyright © 2018 Gra7. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

let hostName = "http://za.com.sa/homy/public/api/"

enum ServiceName: String {
    
    case login = "registerAndLogin"
    case category = "category"
    case subCategory = "subCategory_materials"
    case companies = "material_companies"
    case companyDetails = "company_material"
    case cart = "cart"
    case search = "search"
    case increaseCart = "increase_cart"
    case decreaseCart = "decrease_cart"
    case order = "order"
    case orderService = "order_service"
    case complaints = "complaints"
    case profile = "update_profile"
    case logout = "logout"
    case address = "address"
    case checkNumber = "check_mobile"
    case offer = "offers"
    case deleteCart = "delete_cart"
    case rate = "rate"
}

class Manager {
    var lang : String = "ar"
    
    
    func perform(methodType: HTTPMethod = .post, getWithParams: Bool = false, arguments: String? = nil, serviceName: ServiceName, parameters: [String: AnyObject]? = nil, completionHandler: @escaping (Any?, String?) -> Void) -> Void {
        
        var urlString: String = "\(hostName)\(serviceName.rawValue)"
        
        var params: [String: AnyObject]? = parameters
        
        // Method Type Get With Parameters
        
        if getWithParams {
            
            urlString = "\(hostName)\(serviceName.rawValue)?"
            
            if let stringParams = parameters as? [String: String] {
                
                for (index, item) in stringParams.enumerated() {
                    
                    urlString.append(item.key)
                    urlString.append("=")
                    urlString.append(item.value)
                    
                    let lastIndex = (stringParams.count-1)
                    if index != lastIndex {
                        urlString.append("&")
                    }
                    
                }
                
            }
            
            params = nil
            
        }
        
        // URL With Arguments
        if let args = arguments {
            urlString = "\(hostName)\(serviceName.rawValue)/\(args)"
        }
        
        print("URL: \(urlString)")
        print("ServiceName:\(serviceName)  parameters: \(params ?? [:]))")
        
        var headers: HTTPHeaders = ["Accept": "application/json",
                                    "Content-Type": "application/json",
                                    "Accept-Language": L102Language.currentAppleLanguage()]
        
        if User.shared.token != nil {
            headers["Authorization"] = "Bearer" + " " + (User.shared.token!)
        }
        
        print("Headers: \(headers)")
        
        Alamofire.request(urlString, method: methodType, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
            
            debugPrint(response)
            
            if response.result.isSuccess {
                
                let responseValue = response.result.value!
                
                let statusCode = response.response?.statusCode
                
                if  let responseDict = responseValue as? [String: Any] {
                    print("Response: \(responseDict)")
                }
                
                if statusCode! >= 200 && statusCode! <= 300 {
                    
                    if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                        
                        if base.status == false {
                            if let errors = base.message?.values.first {
                                if errors.count > 0 {
                                    if let error = errors.first {
                                        completionHandler(nil, error)
                                    }
                                }
                            }else {
                                completionHandler(nil, "Something Went Wrong".localized)
                            }
                            
                        }else {
                            
                            completionHandler(base.data, nil)
                            
                        }
                        
                    }
                    
                }else{
                    
                    if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                        if let errors = base.message?.values.first {
                            if errors.count > 0 {
                                if let error = errors.first {
                                    completionHandler(nil, error)
                                }
                            }
                        }else {
                            completionHandler(nil, "Something Went Wrong".localized)
                        }
                    }
                    
                }
                
            } else { //FAILURE
                
                print("error \(String(describing: response.result.error)) in serviceName: \(serviceName)")
                completionHandler(nil, "Something Went Wrong".localized)
                
            }
        }
    }
    
    //
    //    func perform(methodType: HTTPMethod = .post, useCustomeURL: Bool = false, urlStr: String = "", serviceName: ServiceName, parameters: [String: AnyObject]? = nil, completionHandler: @escaping (Any?, String?) -> Void)-> Void {
    //
    //        if L102Language.currentAppleLanguage() == arabicLang {
    //            self.lang = "ar"
    //        }else{
    //            self.lang = "en"
    //        }
    //
    //        var urlString: String = ""
    //        var headers: HTTPHeaders? = nil
    //
    //        if useCustomeURL {
    //            urlString = urlStr
    //        }else {
    //            urlString = "\(hostName)\(serviceName.rawValue)"
    //        }
    //
    //
    //        print("ServiceName:\(serviceName)  parameters: \(String(describing: parameters))")
    //
    //
    //        if User.shared.token != "" && User.shared.token != nil{
    //            headers = [
    //                "Accept-Language": self.lang,
    //                "Authorization": "Bearer \(User.shared.token!)"
    //            ]
    //        }else{
    //            headers = [
    //                "Accept-Language": self.lang
    //            ]
    //        }
    //
    //
    //        Alamofire.request(urlString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{ response in
    //
    //
    //            debugPrint(response)
    //
    //            if response.result.isSuccess {
    //
    //                let dict = response.result.value! as! [String: Any]
    //
    //                if dict["value"] as? Bool == true || dict["value"] as? String == "true" {
    //                    completionHandler(dict, nil)
    //                }else{
    //                    if let dictError = dict["error"] as? String {
    //                        completionHandler(nil, dictError)
    //                    }else {
    //                        guard let errorStr = dict["msg"] as? String else {
    //                            let errorsDict = dict["msg"] as! [String: Any]
    //                            let errorsArr = errorsDict.values.first as! [String]
    //
    //                            completionHandler(nil, errorsArr[0])
    //                            return
    //                        }
    //                        completionHandler(nil, errorStr)
    //                    }
    //                }
    //
    //            } else { //FAILURE
    //                print("error \(String(describing: response.result.error)) in serviceName: \(serviceName)")
    //                completionHandler(nil, response.result.error?.localizedDescription)
    //            }
    //        }
    //    }
    
    
    
    func uploadImage(serviceName: ServiceName, profileImage: UIImage? = nil, parameters: [String: AnyObject]? = nil, progress: @escaping (_ percent: Float) -> Void, completionHandler: @escaping (Any?, String?) -> Void) {
        
        var headers: HTTPHeaders? = nil
        
        if User.shared.token != "" && User.shared.token != nil{
            headers = [
                "Accept-Language": "en",
                "Authorization": "bearer \(User.shared.token!)"
            ]
        }else{
            headers = [
                "Accept-Language": "en"
            ]
        }
        
        let urlString: String =  "\(hostName)\(serviceName.rawValue)"
        
        
        
        let URL = try! URLRequest(url: urlString, method: .post, headers: headers)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                if(profileImage != nil ){
                    if  let profileImageData = profileImage?.jpegData(compressionQuality: 0.5) {
                        multipartFormData.append(profileImageData , withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                    }
                }
                
        },
            with: URL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let request, _, _):
                    print("success")
                    request.uploadProgress(closure: { (progress_) in
                        print("Upload Progress: \(progress_.fractionCompleted)")
                        print("Upload totalUnitCount: \(progress_.totalUnitCount)")
                    })
                    request.validate(statusCode: 200..<500)
                    request.responseJSON { response in
                        
                        if response.result.isSuccess {
                            
                            let responseValue = response.result.value!
                            
                            let statusCode = response.response?.statusCode
                            
                            if  let responseDict = responseValue as? [String: Any] {
                                print("Response: \(responseDict)")
                            }
                            
                            if statusCode! >= 200 && statusCode! <= 300 {
                                
                                if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                                    
                                    if base.status == false {
                                        if let errors = base.message?.values.first {
                                            if errors.count > 0 {
                                                if let error = errors.first {
                                                    completionHandler(nil, error)
                                                }
                                            }
                                        }else {
                                            completionHandler(nil, "Something Went Wrong".localized)
                                        }
                                        
                                    }else {
                                        
                                        completionHandler(base.data, nil)
                                        
                                    }
                                    
                                }
                                
                            }else{
                                
                                if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                                    if let errors = base.message?.values.first {
                                        if errors.count > 0 {
                                            if let error = errors.first {
                                                completionHandler(nil, error)
                                            }
                                        }
                                    }else {
                                        completionHandler(nil, "Something Went Wrong".localized)
                                    }
                                }
                                
                            }
                            
                        } else { //FAILURE
                            
                            print("error \(String(describing: response.result.error)) in serviceName: \(serviceName)")
                            completionHandler(nil, "Something Went Wrong".localized)
                            
                        }
                        
                    }
                case .failure(let errorType):
                    print("encodingError:\(errorType)")
                }
        })
        
    }
    func performJson(methodType: HTTPMethod = .post, useCustomeURL: Bool = false, urlStr: String = "", serviceName: ServiceName, parameters: [String: Any]? = nil, completionHandler: @escaping (Any?, String?) -> Void)-> Void {
        
        var urlString: String = ""
        var headers: HTTPHeaders? = nil
        
        if useCustomeURL {
            urlString = urlStr
        }else {
            urlString = "\(hostName)\(serviceName.rawValue)"
        }
        
        
        print("ServiceName:\(serviceName)  parameters: \(String(describing: parameters))")
        
        
        if User.shared.token != "" && User.shared.token != nil{
            headers = [
                "Accept-Language": "en",
                "Authorization": "Bearer \(User.shared.token!)"
            ]
        }else{
            headers = [
                "Accept-Language": "en"
            ]
        }
        
        
        Alamofire.request(urlString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON{  response in
            
            debugPrint(response)
            
            if response.result.isSuccess {
                
                let responseValue = response.result.value!
                
                let statusCode = response.response?.statusCode
                
                if  let responseDict = responseValue as? [String: Any] {
                    print("Response: \(responseDict)")
                }
                
                if statusCode! >= 200 && statusCode! <= 300 {
                    
                    if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                        
                        if base.status == false {
                            if let errors = base.message?.values.first {
                                if errors.count > 0 {
                                    if let error = errors.first {
                                        completionHandler(nil, error)
                                    }
                                }
                            }else {
                                completionHandler(nil, "Something Went Wrong".localized)
                            }
                            
                        }else {
                            
                            completionHandler(base.data, nil)
                            
                        }
                        
                    }
                    
                }else{
                    
                    if let base: Base = Mapper<Base>().map(JSONObject: responseValue) {
                        if let errors = base.message?.values.first {
                            if errors.count > 0 {
                                if let error = errors.first {
                                    completionHandler(nil, error)
                                }
                            }
                        }else {
                            completionHandler(nil, "Something Went Wrong".localized)
                        }
                    }
                    
                }
                
            } else { //FAILURE
                
                print("error \(String(describing: response.result.error)) in serviceName: \(serviceName)")
                completionHandler(nil, "Something Went Wrong".localized)
                
            }
        }
    }
    
    
}
